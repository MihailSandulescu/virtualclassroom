/**
 * @packageDocumentation
 * @module UI/User
 */

import {UserDTO} from "../../../api/modules/user/models/UserDTO";
import {AccessRole} from "../../../api/modules/role/models/Role";
import {IsEmail, IsEnum, IsISO8601, IsNumber, IsPhoneNumber, IsString} from "class-validator";

export class UIUserDTO implements UserDTO {
    @IsString()
    public firstName: string;

    @IsString()
    public lastName: string;

    @IsString()
    public password: string;

    @IsISO8601()
    public birthDate: Date;

    @IsEmail()
    public email: string;

    @IsPhoneNumber("ZZ")
    public phone: string;

    @IsNumber()
    public startYear: number;

    @IsEnum(AccessRole)
    public role: AccessRole;

    public faculty?: any;

    public group?: any;
}