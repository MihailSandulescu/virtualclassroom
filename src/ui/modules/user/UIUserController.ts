/**
 * @packageDocumentation
 * @module UI/User
 */

import {Controller, Get, Param, Render, UseFilters, UseGuards} from "@nestjs/common";
import {HttpExceptionFilter} from "../../interceptors/HttpExceptionFilter";
import {LoggedUser} from "../../../api/decorators/LoggedUser";
import {UserService} from "../../../api/modules/user/service/UserService";
import {User} from "../../../api/modules/user/models/User";
import {Group} from "../../../api/modules/group/models/Group";
import {GroupService} from "../../../api/modules/group/service/GroupService";
import {FacultyService} from "../../../api/modules/faculty/service/FacultyService";
import {Faculty} from "../../../api/modules/faculty/models/Faculty";
import {AuthenticatedGuard} from "../../../api/modules/authentication/models/guards/AuthenticatedGuard";
import {Utils} from "../../../utils/Utils";
import {AssignmentSolutionService} from "../../../api/modules/assignment-solution/service/AssignmentSolutionService";
import {AssignmentSolution} from "../../../api/modules/assignment-solution/models/AssignmentSolution";

/**
 * Controller for handling the `/users` route
 */
@Controller("/users")
@UseFilters(HttpExceptionFilter)
@UseGuards(AuthenticatedGuard)
export class UIUserController {

    constructor(
        private readonly userService: UserService,
        private readonly groupService: GroupService,
        private readonly facultyService: FacultyService,
        private readonly assignmentSolutionService: AssignmentSolutionService
    ) {}

    /**startDate
     * Render my profile.
     *
     * Handle GET `/users/me`.
     */
    @Get("me")
    @Render("pages/users/item")
    public async renderMyProfile(
        @LoggedUser() user: User,
        @Param('id') userId: string
    ): Promise<any> {
        const userGroup: Group = await this.groupService.getUserGroup(user._id.toHexString(), true);
        const userFaculty: Faculty = await this.facultyService.getGroupFaculty(userGroup._id.toHexString());
        const assignmentSolutions: AssignmentSolution[] = await this.assignmentSolutionService.getAssignmentSolutionsForUser(user._id.toHexString());

        return {
            user,
            assignmentSolutions,
            group: userGroup,
            faculty: userFaculty,
            generateColor: Utils.getStringColor
        };
    }

    /**
     * Render a user's profile.
     *
     * Handle GET `/users/:id`.
     */
    @Get(":id")
    @Render("pages/users/item")
    public async renderUserProfile(@Param('id') userId: string): Promise<any> {
        const userPromise: Promise<User> = this.userService.getUser(userId);
        const userGroupPromise: Promise<Group> = this.groupService.getUserGroup(userId, true);
        const assignmentSolutionsPromise: Promise<AssignmentSolution[]> = this.assignmentSolutionService.getAssignmentSolutionsForUser(userId);

        const [user, userGroup, assignmentSolutions]: [User, Group, AssignmentSolution[]] = await Promise.all([
            userPromise, userGroupPromise, assignmentSolutionsPromise
        ]);

        const userFaculty: Faculty = await this.facultyService.getGroupFaculty(userGroup._id.toHexString());

        return {
            user,
            assignmentSolutions,
            group: userGroup,
            faculty: userFaculty,
            generateColor: Utils.getStringColor
        };
    }

}