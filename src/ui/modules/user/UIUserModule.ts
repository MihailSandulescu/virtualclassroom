/**
 * NestJS Module: **UIUserModule**
 *
 * Controllers: [[UIUserController]]
 *
 * Imports: [[UserModule]], [[GroupModule]], [[FacultyModule]], [[AssignmentSolutionModule]]
 * @packageDocumentation
 * @module UI/User
 * @preferred
 */

import {Module} from "@nestjs/common";
import {UIUserController} from "./UIUserController";
import {UserModule} from "../../../api/modules/user/UserModule";
import {GroupModule} from "../../../api/modules/group/GroupModule";
import {FacultyModule} from "../../../api/modules/faculty/FacultyModule";
import {AssignmentSolutionModule} from "../../../api/modules/assignment-solution/AssignmentSolutionModule";

/**
 * NestJS Module: **UIUserModule**
 *
 * Controllers: [[UIUserController]]
 *
 * Imports: [[UserModule]], [[GroupModule]], [[FacultyModule]], [[AssignmentSolutionModule]]
 */
@Module({
    controllers: [UIUserController],
    imports: [UserModule, GroupModule, FacultyModule, AssignmentSolutionModule]
})
export class UIUserModule {}