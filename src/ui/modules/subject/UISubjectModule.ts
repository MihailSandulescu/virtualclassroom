/**
 * NestJS Module: **UISubjectModule**
 *
 * Controllers: [[UISubjectController]]
 *
 * Imports: [[SubjectModule]], [[AssignmentsModule]], [[FacultyModule]], [[GroupModule]]
 * @packageDocumentation
 * @module UI/Subject
 * @preferred
 */

import {Module} from "@nestjs/common";
import {UISubjectController} from "./UISubjectController";
import {SubjectModule} from "../../../api/modules/subject/SubjectModule";
import {AssignmentsModule} from "../../../api/modules/assignments/AssignmentsModule";
import {FacultyModule} from "../../../api/modules/faculty/FacultyModule";
import {GroupModule} from "../../../api/modules/group/GroupModule";

/**
 * NestJS Module: **UIUserModule**
 *
 * Controllers: [[UIUserController]]
 *
 * Imports: [[SubjectModule]], [[AssignmentsModule]], [[FacultyModule]], [[GroupModule]]
 */
@Module({
    controllers: [UISubjectController],
    imports: [SubjectModule, AssignmentsModule, FacultyModule, GroupModule]
})
export class UISubjectModule {}