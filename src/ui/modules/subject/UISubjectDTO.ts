/**
 * @packageDocumentation
 * @module UI/User
 */

import {SubjectDTO} from "../../../api/modules/subject/models/SubjectDTO";
import {IsNumber, IsOptional, IsString, ValidateNested} from "class-validator";
import {Type} from "class-transformer";
import {SubjectTimetableDTO} from "../../../api/modules/subject/models/SubjectTimetableDTO";
import {ISubjectTimetable} from "../../../api/modules/subject/models/ISubject";

export class UISubjectDTO implements SubjectDTO {
    @IsString()
    public name: string;

    @IsNumber()
    public year: number;

    @ValidateNested()
    @Type(() => SubjectTimetableDTO)
    public timetable: ISubjectTimetable;

    @IsString()
    public semester: string;

    @IsOptional()
    @IsNumber()
    public credits?: number;

    @IsString()
    public professor: string;

    @IsString({ each: true })
    public assignments: string[];
}