/**
 * @packageDocumentation
 * @module UI/Subjects
 */

import {Controller, Get, Param, Render, UseFilters, UseGuards} from "@nestjs/common";
import {HttpExceptionFilter} from "../../interceptors/HttpExceptionFilter";
import {LoggedUser} from "../../../api/decorators/LoggedUser";
import {User} from "../../../api/modules/user/models/User";
import {AuthenticatedGuard} from "../../../api/modules/authentication/models/guards/AuthenticatedGuard";
import {SubjectService} from "../../../api/modules/subject/service/SubjectService";
import {AssignmentsService} from "../../../api/modules/assignments/service/AssignmentsService";
import {Subject} from "../../../api/modules/subject/models/Subject";
import {Assignment} from "../../../api/modules/assignments/models/Assignment";
import {Utils} from "../../../utils/Utils";
import {Group} from "../../../api/modules/group/models/Group";
import {Faculty} from "../../../api/modules/faculty/models/Faculty";
import {GroupService} from "../../../api/modules/group/service/GroupService";
import {FacultyService} from "../../../api/modules/faculty/service/FacultyService";

/**
 * Controller for handling the `/subjects` route
 */
@Controller("/subjects")
@UseFilters(HttpExceptionFilter)
@UseGuards(AuthenticatedGuard)
export class UISubjectController {

    constructor(
        private readonly subjectService: SubjectService,
        private readonly assignmentService: AssignmentsService,
        private readonly groupService: GroupService,
        private readonly facultyService: FacultyService
    ) {}

    /**
     * Render all Subjects
     *
     * Handle GET `/subjects`.
     */
    @Get("/")
    @Render("pages/subjects/list")
    public async renderSubjectList(
        @LoggedUser() user: User
    ): Promise<any> {
        const subjects: Subject[] = await this.subjectService.getSubjects();

        return {
            user,
            subjects,
            generateColor: Utils.getStringColor
        };
    }

    /**
     * Render a Subject's details
     *
     * Handle GET `/subjects/:id`.
     */
    @Get(":id")
    @Render("pages/subjects/item")
    public async renderSubject(
        @LoggedUser() user: User,
        @Param('id') subjectId: string
    ): Promise<any> {
        const subject: Subject = await this.subjectService.getSubject(subjectId);
        const assignments: Assignment[] = await this.assignmentService.getAssignmentsForSubject(subjectId);
        const userGroup: Group = await this.groupService.getUserGroup(user._id.toHexString());
        const userFaculty: Faculty = await this.facultyService.getGroupFaculty(userGroup._id.toHexString());

        return {
            user,
            subject,
            assignments,
            group: userGroup,
            faculty: userFaculty,
            generateColor: Utils.getStringColor
        };
    }

}