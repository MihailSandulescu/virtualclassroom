/**
 * @packageDocumentation
 * @module UI/Login
 */

import {
    Body,
    Controller,
    Get,
    Post,
    Render,
    Req,
    Res,
    UseFilters,
    UseGuards,
    UseInterceptors,
    UsePipes,
    ValidationPipe
} from "@nestjs/common";
import {HttpExceptionFilter} from "../../interceptors/HttpExceptionFilter";
import {Request, Response} from "express";
import {AuthenticationService} from "../../../api/modules/authentication/service/AuthenticationService";
import {UILoginDTO} from "./UILoginDTO";
import {ExceptionFactory} from "../../exceptions/ExceptionFactory";
import {ValidationErrorInterceptor} from "../../interceptors/ValidationErrorInterceptor";
import {ValidationException} from "../../exceptions/ValidationException";
import {OptionalAuthGuard} from "../../../api/modules/authentication/models/guards/OptionalAuthGuard";
import {LoggedUser} from "../../../api/decorators/LoggedUser";
import {User} from "../../../api/modules/user/models/User";

/**
 * Controller for handling the root `/` route
 */
@Controller("/login")
@UseFilters(HttpExceptionFilter)
export class UILoginController {

    constructor(
        private readonly authenticationService: AuthenticationService
    ) {}

    /**
     * Render the login page.
     *
     * Handle GET `/login`.
     */
    @Get()
    @UseGuards(OptionalAuthGuard)
    @Render("pages/login")
    public async renderLogin(
        @LoggedUser() user: User | undefined,
        @Res() res: Response
    ): Promise<void> {
        if (user) {
            res.redirect("/");
        }
    }

    /**
     * Render the login page.
     *
     * Handle POST `/login`.
     */
    @Post()
    @UseInterceptors(new ValidationErrorInterceptor("pages/login"))
    @UsePipes(new ValidationPipe({
        whitelist: true,
        exceptionFactory: ExceptionFactory
    }))
    public async handleLogin(
        @Body() loginDto: UILoginDTO,
        @Req() req: Request,
        @Res() res: Response
    ): Promise<void> {
        try {
            const token: string = await this.authenticationService.authenticate(loginDto.identifier, loginDto.password);

            res.cookie("session_token", token, {
                httpOnly: true,
                sameSite: "lax",
                expires: loginDto.remember === "on" ? new Date(Date.now() + 2.628e+9) : undefined
            });

            if (req.cookies.tmp_redirect) {
                const redirectUrl: string = req.cookies.tmp_redirect;

                res.clearCookie("tmp_redirect");
                res.redirect(redirectUrl);
            } else {
                res.redirect("/");
            }
        } catch (e) {
            throw new ValidationException(e.message, { identifier: "Wrong identifier or password" })
        }
    }

    /**
     * Logs the user out
     *
     * Handle GET `/login/out`.
     */
    @Get("out")
    public async handleLogout(
        @Req() req: Request,
        @Res() res: Response
    ): Promise<void> {
        res.clearCookie("session_token");
        res.redirect("/login");
    }


}