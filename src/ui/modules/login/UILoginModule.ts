/**
 * NestJS Module: **UILoginModule**
 *
 * Controllers: [[UILoginController]]
 *
 * Imports: [[AuthenticationModule]]
 * @packageDocumentation
 * @module UI/Login
 * @preferred
 */

import { Module } from "@nestjs/common";
import { UILoginController } from "./UILoginController";
import {AuthenticationModule} from "../../../api/modules/authentication/AuthenticationModule";

/**
 * NestJS Module: **UILoginModule**
 *
 * Controllers: [[UILoginController]]
 *
 * Imports: [[AuthenticationModule]]
 */
@Module({
    controllers: [UILoginController],
    imports: [AuthenticationModule]
})
export class UILoginModule {}