/**
 * @packageDocumentation
 * @module UI/Login
 */

import {IsOptional, IsString} from "class-validator";
import {LoginDTO} from "../../../api/modules/authentication/models/LoginDTO";

export class UILoginDTO implements LoginDTO {
    @IsString()
    public identifier: string;

    @IsString()
    public password: string;

    @IsOptional()
    @IsString()
    public remember: string;
}