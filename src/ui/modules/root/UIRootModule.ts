/**
 * NestJS Module: **UIRootModule**
 *
 * Controllers: [[UIRootController]]
 *
 * Imports: [[GroupModule]], [[FacultyModule]]
 * @packageDocumentation
 * @module UI/Root
 * @preferred
 */

import { Module } from "@nestjs/common";
import { UIRootController } from "./UIRootController";
import {GroupModule} from "../../../api/modules/group/GroupModule";
import {FacultyModule} from "../../../api/modules/faculty/FacultyModule";

/**
 * NestJS Module: **UIRootModule**
 *
 * Controllers: [[UIRootController]]
 *
 * Imports: [[GroupModule]], [[FacultyModule]]
 */
@Module({
    controllers: [UIRootController],
    imports: [GroupModule, FacultyModule]
})
export class UIRootModule {}