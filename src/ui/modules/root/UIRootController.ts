/**
 * @packageDocumentation
 * @module UI/Root
 */

import {Controller, Get, Render, UseFilters, UseGuards} from "@nestjs/common";
import {HttpExceptionFilter} from "../../interceptors/HttpExceptionFilter";
import {LoggedUser} from "../../../api/decorators/LoggedUser";
import {GroupService} from "../../../api/modules/group/service/GroupService";
import {FacultyService} from "../../../api/modules/faculty/service/FacultyService";
import {Group} from "../../../api/modules/group/models/Group";
import {Faculty} from "../../../api/modules/faculty/models/Faculty";
import {User} from "../../../api/modules/user/models/User";
import {AuthenticatedGuard} from "../../../api/modules/authentication/models/guards/AuthenticatedGuard";

/**
 * Controller for handling the root `/` route
 */
@Controller("/")
@UseFilters(HttpExceptionFilter)
@UseGuards(AuthenticatedGuard)
export class UIRootController {

    constructor(
        private readonly groupService: GroupService,
        private readonly facultyService: FacultyService
    ) {}

    /**
     * Render the homepage.
     *
     * Handle GET `/`.
     */
    @Get()
    @Render("pages/home")
    public async renderRoot(@LoggedUser() user: User): Promise<any> {
        const userGroup: Group = await this.groupService.getUserGroup(user._id.toHexString(), true);
        const userFaculty: Faculty = await this.facultyService.getGroupFaculty(userGroup._id.toHexString());

        return {
            user,
            faculty: userFaculty
        };
    }
}