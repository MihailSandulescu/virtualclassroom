/**
 * @packageDocumentation
 * @module UI/Assignments
 */

import {
    BadRequestException,
    Controller,
    Get,
    Param,
    Post,
    Render,
    UploadedFile,
    UseFilters,
    UseGuards,
    UseInterceptors
} from "@nestjs/common";
import {HttpExceptionFilter} from "../../interceptors/HttpExceptionFilter";
import {LoggedUser} from "../../../api/decorators/LoggedUser";
import {User} from "../../../api/modules/user/models/User";
import {AuthenticatedGuard} from "../../../api/modules/authentication/models/guards/AuthenticatedGuard";
import {SubjectService} from "../../../api/modules/subject/service/SubjectService";
import {AssignmentsService} from "../../../api/modules/assignments/service/AssignmentsService";
import {Subject} from "../../../api/modules/subject/models/Subject";
import {Assignment} from "../../../api/modules/assignments/models/Assignment";
import {Utils} from "../../../utils/Utils";
import {Group} from "../../../api/modules/group/models/Group";
import {Faculty} from "../../../api/modules/faculty/models/Faculty";
import {GroupService} from "../../../api/modules/group/service/GroupService";
import {FacultyService} from "../../../api/modules/faculty/service/FacultyService";
import {AccessRole, Role} from "../../../api/modules/role/models/Role";
import {AssignmentFileInterceptor} from "../../../api/modules/assignments/interceptor/AssignmentFileInterceptor";
import path from "path";
import {AssignmentSolutionDTO} from "../../../api/modules/assignment-solution/models/dto/AssignmentSolutionDTO";
import {ObjectId} from "bson";
import {AssignmentSolutionService} from "../../../api/modules/assignment-solution/service/AssignmentSolutionService";
import {AssignmentSolution} from "../../../api/modules/assignment-solution/models/AssignmentSolution";

/**
 * Controller for handling the `/subjects` route
 */
@Controller("/assignments")
@UseFilters(HttpExceptionFilter)
@UseGuards(AuthenticatedGuard)
export class UIAssignmentController {

    constructor(
        private readonly subjectService: SubjectService,
        private readonly assignmentService: AssignmentsService,
        private readonly assignmentSolutionService: AssignmentSolutionService,
        private readonly groupService: GroupService,
        private readonly facultyService: FacultyService
    ) {}

    /**
     * Render all Assignments
     *
     * Handle GET `/assignments`.
     */
    @Get("/")
    @Render("pages/assignments/list")
    public async renderAssignmentList(
        @LoggedUser() user: User
    ): Promise<any> {
        const vars: any = {
            user,
            generateColor: Utils.getStringColor
        };

        const role: Role = user.role as Role;

        if (role.roleLevel === AccessRole.ProfessorRole || role.roleLevel === AccessRole.AdminRole) {
            const assignments: Assignment[] = await this.assignmentService.getAssignmentsForProfessor(user._id.toHexString());
            Object.assign(vars, { assignments });
        } else {
            const assignments: Assignment[] = await this.assignmentService.getAssignmentsForUser(user._id.toHexString());
            Object.assign(vars, { assignments });
        }

        return vars
    }

    /**
     * Render a Assignment's details
     *
     * Handle GET `/assignments/:id`.
     */
    @Get(":id")
    @Render("pages/assignments/item")
    public async renderAssignment(
        @LoggedUser() user: User,
        @Param('id') assignmentId: string
    ): Promise<any> {
        const vars: any = {};

        const assignment: Assignment = await this.assignmentService.getAssignment(assignmentId);
        const subject: Subject = await this.subjectService.getSubject(assignment.subject as string);
        const userGroup: Group = await this.groupService.getUserGroup(user._id.toHexString());
        const userFaculty: Faculty = await this.facultyService.getGroupFaculty(userGroup._id.toHexString());

        const role: Role = user.role as Role;
        if (role.roleLevel === AccessRole.StudentRole) {
            const solution: AssignmentSolution | null = await this.assignmentSolutionService.getAssignmentSolutionsForAssignmentAndUser(
                assignmentId,
                user._id.toHexString()
            );

            if (solution) {
                Object.assign(vars, {
                    solution
                });
            }
        }

        if (role.roleLevel === AccessRole.ProfessorRole || role.roleLevel === AccessRole.AdminRole) {
            const solutions: AssignmentSolution[] = await this.assignmentSolutionService.getAssignmentSolutionsForAssignment(
                assignmentId
            );

            Object.assign(vars, {
                solutions
            });
        }

        return {
            ...vars,
            user,
            assignment,
            subject,
            group: userGroup,
            faculty: userFaculty,
            generateColor: Utils.getStringColor
        };
    }

    /**
     * Handle upload for the assignment solution
     * @param user - the logged in user
     * @param id- the assignment id
     * @param file - file being uploaded
     */
    @Post(":id/solution-upload")
    @UseInterceptors(AssignmentFileInterceptor("file"))
    public async handleAssignmentSolutionUpload(
        @LoggedUser() user: User,
        @Param('id') id: string,
        @UploadedFile() file: Express.Multer.File,
    ): Promise<any> {
        if (!ObjectId.isValid(id)) {
            throw new BadRequestException("The provided ID is not a valid ObjectId.");
        }

        const assignment: Assignment = await this.assignmentService.getAssignment(id);

        let segments: string[] = file.destination.split("/");
        segments = segments.slice(Math.max(segments.length - 3, 0));

        const assignmentSolutionDto: AssignmentSolutionDTO = new AssignmentSolutionDTO();
        Object.assign(assignmentSolutionDto, {
            file: path.join(...segments, file.filename),
            assignment: assignment._id.toHexString(),
            student: user._id.toHexString()
        });

        return this.assignmentSolutionService.addAssignmentSolution(assignmentSolutionDto);
    }

}