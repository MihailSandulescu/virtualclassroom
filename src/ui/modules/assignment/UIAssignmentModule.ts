/**
 * NestJS Module: **UISubjectModule**
 *
 * Controllers: [[UISubjectController]]
 *
 * Imports: [[SubjectModule]], [[AssignmentsModule]], [[FacultyModule]], [[GroupModule]], [[AssignmentSolutionModule]]
 * @packageDocumentation
 * @module UI/Subject
 * @preferred
 */

import {Module} from "@nestjs/common";
import {UIAssignmentController} from "./UIAssignmentController";
import {SubjectModule} from "../../../api/modules/subject/SubjectModule";
import {AssignmentsModule} from "../../../api/modules/assignments/AssignmentsModule";
import {FacultyModule} from "../../../api/modules/faculty/FacultyModule";
import {GroupModule} from "../../../api/modules/group/GroupModule";
import {AssignmentSolutionModule} from "../../../api/modules/assignment-solution/AssignmentSolutionModule";

/**
 * NestJS Module: **UIUserModule**
 *
 * Controllers: [[UIUserController]]
 *
 * Imports: [[SubjectModule]], [[AssignmentsModule]], [[FacultyModule]], [[GroupModule]], [[AssignmentSolutionModule]]
 */
@Module({
    controllers: [UIAssignmentController],
    imports: [SubjectModule, AssignmentsModule, FacultyModule, GroupModule, AssignmentSolutionModule]
})
export class UIAssignmentModule {}