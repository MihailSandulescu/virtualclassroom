/*
 * This file is part of the Arnapou jqCron package.
 *
 * (c) Arnaud Buathier <arnaud@arnapou.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

jqCronDefaultSettings.texts.ro = {
    empty: 'in fiecare',
    empty_minutes: 'in fiecare',
    empty_time_hours: 'in fiecare ora',
    empty_time_minutes: 'in fiecare minut',
    empty_day_of_week: 'in fiecare zi a saptamanii',
    empty_day_of_month: 'in fiecare zi a lunii',
    empty_month: 'in fiecare luna',
    name_minute: 'minut',
    name_hour: 'ora',
    name_day: 'zi',
    name_week: 'saptamana',
    name_month: 'luna',
    name_year: 'an',
    text_period: 'In fiecare <b />',
    text_mins: ' la si <b /> minute',
    text_time: ' la <b />:<b />',
    text_dow: ' pe <b />',
    text_month: ' al <b />',
    text_dom: ' pe <b />',
    error1: 'Tagul %s nu este suportat !',
    error2: 'Numar gresit de elemente',
    error3: 'The jquery_element should be set into jqCron settings',
    error4: 'Unrecognized expression',
    weekdays: ['luni', 'marti', 'miercuri', 'joi', 'vineri', 'sambata', 'duminica'],
    months: ['ianuarie', 'februarie', 'martie', 'aprilie', 'mai', 'junie', 'iulie', 'august', 'septembrie', 'octombrie', 'noiembrie', 'decembrie']
};
