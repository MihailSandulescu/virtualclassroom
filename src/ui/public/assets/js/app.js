/* ------------------------------------------------------------------------------
*  # Main JS file
*  # The main js file is common for all demos
* ---------------------------------------------------------------------------- */

// CORE APP OBJECT
// ======================

var APP = function() {
    this.ASSETS_PATH = './assets/';
};

var APP = new APP();

// APP UI SETTINGS

APP.UI = {
	scrollTop: 0, // Minimal scrolling to show scrollTop button
};


// PAGE PRELOADING ANIMATION
$(window).on('load', function() {
	setTimeout(function() {
		$('.preloader-backdrop').fadeOut(200);
		$('body').addClass('has-animation').trigger('load');
	},0);
});



$(function () {

    // BACK TO TOP
    $(window).scroll(function () {
        if ($(this).scrollTop() > APP.UI.scrollTop) $('.to-top').fadeIn();
        else $('.to-top').fadeOut();
    });
    $('.to-top').click(function (e) {
        $("html, body").animate({scrollTop: 0}, 500);
    });

    // PANEL ACTIONS
    // ======================

    $('.ibox-collapse').click(function () {
        var ibox = $(this).closest('div.ibox');
        ibox.toggleClass('collapsed-mode').children('.ibox-body').slideToggle(200);
    });
    $('.ibox-remove').click(function () {
        $(this).closest('div.ibox').remove();
    });

    // Activate Tooltips
    $('[data-toggle="tooltip"]').tooltip();

    // Activate Popovers
    $('[data-toggle="popover"]').popover();

    // Activate slimscroll
    $('.scroller').each(function () {
        $(this).slimScroll({
            height: $(this).attr('data-height') || '100%',
            color: $(this).attr('data-color') || '#71808f',
            railOpacity: '0.9',
            size: '4px',
        });
    });

    $('.slimScrollBar').hide();


    // Backdrop functional

    $.fn.backdrop = function () {
        $(this).toggleClass('shined');
        $('body').toggleClass('has-backdrop');
        return $(this);
    };

    $('.backdrop').click(closeShined);

    function closeShined() {
        $('body').removeClass('has-backdrop');
        $('.shined').removeClass('shined');
    }
});

// LAYOUT SETTINGS
// ======================

$(window).on('load resize', function () {
    if ($(this).width() < 992) $('body').addClass('drawer-sidebar');
    else $('body').removeClass('drawer-sidebar');
});


$(function () {
    const body = $("body");

    // LAYOUT STYLE
    $("[name='layout-style']").change(function(){
        if(+$(this).val()) $('body').addClass('boxed-layout');
        else $('body').removeClass('boxed-layout');
    });

    $(".js-sidebar-toggler").click(function () {
        body.hasClass("drawer-sidebar") ? $("#sidebar").backdrop() && $("header").addClass("shined") : body.toggleClass("sidebar-mini")
    });

    $("#_fixedNavbar").change(function () {
        $(this).is(":checked") ? body.addClass("fixed-navbar") : body.removeClass("fixed-navbar")
    });

    $("#_drawerSidebar").change(function () {
        $(this).is(":checked") ? body.addClass("drawer-sidebar") : body.removeClass("drawer-sidebar"),
            setTimeout(function () {
                $("body").removeClass("sidebar-mini")
            }, 200)
    });

    // Navbar dropdowns
    $('.top-navbar .dropdown-menu .has-submenu > a').click(function(){
        $(this).parent().toggleClass('open').siblings('li.open').removeClass('open');
        return false;
    });

});
