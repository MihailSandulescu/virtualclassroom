/**
 * @packageDocumentation
 * @module UI
 */


import { BadRequestException } from "@nestjs/common";

export class ValidationException extends BadRequestException {
    public error: { [key: string]: string; };

    constructor(message: string, error: { [key: string]: string; }) {
        super();
        this.message = message;
        this.error = error;
    }
}