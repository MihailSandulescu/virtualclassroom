/**
 * @packageDocumentation
 * @module UI
 */


import { ValidationError } from "@nestjs/common";
import { ValidationException } from "./ValidationException";

export const ExceptionFactory: ExceptionFactory = (errors: ValidationError[]) => {
    const findNested: (errors: ValidationError[]) => any = (errors: ValidationError[]) => {
        const errorTree: { [key: string]: string; } = {};

        for (const e of errors) {
            if (e.constraints && e.children.length === 0) {
                Object.assign(errorTree, {
                    [e.property]: Object.values(e.constraints)
                });
            } else {
                Object.assign(errorTree, {
                    [e.property]: findNested(e.children)
                });
            }
        }

        return errorTree;
    }

    const tree: any = findNested(errors);

    return new ValidationException("Validation Error", tree);
};

export type ExceptionFactory = (errors: ValidationError[]) => ValidationException;