/**
 * NestJS Module: **UIModule**
 *
 * Imports:
 *
 * [[UIRootModule]], [[UILoginModule]], [[UIUserModule]], [[UISubjectModule]]
 * @packageDocumentation
 * @module UI
 * @preferred
 */

import {Module} from "@nestjs/common";
import {UIRootModule} from "./modules/root/UIRootModule";
import {UILoginModule} from "./modules/login/UILoginModule";
import {UIUserModule} from "./modules/user/UIUserModule";
import {UISubjectModule} from "./modules/subject/UISubjectModule";
import {UIAssignmentModule} from "./modules/assignment/UIAssignmentModule";

/**
 * NestJS Module: **UIModule**
 *
 * Imports:
 *
 * [[UIRootModule]], [[UILoginModule]], [[UIUserModule]], [[UISubjectModule]]
 */
@Module({
    imports: [
        UIRootModule,
        UILoginModule,
        UIUserModule,
        UISubjectModule,
        UIAssignmentModule
    ]
})
export class UIModule {}