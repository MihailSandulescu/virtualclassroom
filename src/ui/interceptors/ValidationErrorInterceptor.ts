/**
 * @packageDocumentation
 * @module UI/Login
 */

import {CallHandler, ExecutionContext, Injectable, NestInterceptor} from "@nestjs/common";
import {Observable} from "rxjs";
import {catchError} from "rxjs/operators";
import {Request, Response} from "express";
import {ValidationException} from "../exceptions/ValidationException";

export interface IRenderData {
    error: any;
    postData: any;
    [i: string]: any;
}

/**
 * Interceptor that will intercept all ValidationErrors thrown by the [[UITasksModule]]
 * and render the appropriate page with the appropriate errors
 */
@Injectable()
export class ValidationErrorInterceptor implements NestInterceptor {

    constructor(
        private readonly renderPage: string
    ) {}

    /**
     * Intercept a [[ValidationError]] and turn it into a custom object
     * for displaying errors appropriately.
     *
     * @param context
     * @param next
     */
    public intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        const [req, res]: [Request, Response] = context.getArgs();

        return next.handle()
        .pipe(catchError(async (e: Error | ValidationException) => {
            if (!(e instanceof ValidationException)) throw e;
            const errorObj: any = e.error;
            const renderData: IRenderData = {
                error: errorObj,
                postData: req.body,
            };

            return res.render(this.renderPage, renderData);
        }));
    }
}