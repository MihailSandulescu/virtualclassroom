/**
 * @packageDocumentation
 * @module UI
 */

import { ArgumentsHost, Catch, ExceptionFilter, HttpException } from "@nestjs/common";
import { Response } from "express";
import { HttpArgumentsHost } from "@nestjs/common/interfaces";

/**
 * Catch all HttpExceptions thrown by controllers using this filter
 * and render an appropriate error page
 */
@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {

    /**
     * Handle a HttpException and render the appropriate error page
     * @param exception
     * @param host
     */
    public catch(exception: HttpException, host: ArgumentsHost): void {
        const ctx: HttpArgumentsHost = host.switchToHttp();
        const res: Response = ctx.getResponse<Response>();

        switch (exception.getStatus()) {
            case 400:
                return res.render("pages/errors/409", { message: exception.message });
            case 401:
                return res.redirect("/login");
            case 403:
                return res.render("pages/errors/403", { message: exception.message });
            case 404:
                return res.render("pages/errors/404", { message: exception.message });
            default:
                return res.render("pages/errors/500", { message: exception.message });
        }
    }
}