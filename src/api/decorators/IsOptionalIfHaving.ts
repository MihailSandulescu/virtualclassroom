/**
 * @packageDocumentation
 * @module API/Decorators
 */

import { registerDecorator, ValidationArguments, ValidationOptions } from "class-validator";

export function IsOptionalIfHaving(property: string, validationOptions?: ValidationOptions): PropertyDecorator {
    const message: string = "$property: this field is required unless the $constraint1 field is defined";
    if (!validationOptions) validationOptions = { message };
    else Object.assign(validationOptions, { message });

    return function (target: Object, propertyName: string | symbol): void {
        registerDecorator({
            name: "isOptionalIfHaving",
            target: target.constructor,
            propertyName: propertyName as string,
            constraints: [property],
            options: validationOptions,
            validator: {
                validate(value: any, args: ValidationArguments): boolean {
                    const [relatedPropertyName] = args.constraints;
                    const relatedValue: any = (args.object as any)[relatedPropertyName];

                    return (value === undefined && relatedValue !== undefined)
                        || (value !== undefined)
                }
            }
        });
    };
}