/**
 * @packageDocumentation
 * @module API/Decorators
 */

import { registerDecorator, ValidationOptions } from "class-validator";

export function IsOptionalArray(validationOptions?: ValidationOptions): PropertyDecorator {
    const message: string = "$property: this field should be an array";
    if (!validationOptions) validationOptions = { message };
    else Object.assign(validationOptions, { message });

    return function (target: Object, propertyName: string | symbol): void {
        registerDecorator({
            name: "isOptionalArray",
            target: target.constructor,
            propertyName: propertyName as string,
            options: validationOptions,
            validator: {
                validate(value: any): boolean {
                    return value === undefined || Array.isArray(value);
                }
            }
        });
    };
}