/**
 * @packageDocumentation
 * @module API/Decorators
 */

import {createParamDecorator, ExecutionContext} from '@nestjs/common';
import {Request} from "express";

export type ParamDecoratorType = (...dataOrPipes: unknown[]) => ParameterDecorator;

export const LoggedUser: ParamDecoratorType = createParamDecorator(
    (data: unknown, ctx: ExecutionContext) => {
        const request: Request = ctx.switchToHttp().getRequest();
        return request.user;
    },
);