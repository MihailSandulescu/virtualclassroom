/**
 * @packageDocumentation
 * @module API/Decorators
 */

import { registerDecorator, ValidationOptions } from "class-validator";
import cron from "cron-validate";

export function IsCronExpression(validationOptions?: ValidationOptions): PropertyDecorator {
    const message: string = "$property: this field should be a valid CRON expression";
    if (!validationOptions) validationOptions = { message };
    else if (!validationOptions.message) Object.assign(validationOptions, { message });

    return function (target: Object, propertyName: string | symbol): void {
        registerDecorator({
            name: "isCronExpression",
            target: target.constructor,
            propertyName: propertyName as string,
            options: validationOptions,
            validator: {
                validate(value: any): boolean {
                    if (!value) return false;
                    return cron(value, {
                        preset: 'npm-node-cron',
                        override: { useSeconds: false }
                    }).isValid();
                }
            }
        });
    };
}