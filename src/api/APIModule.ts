/**
 * NestJS Module: **APIModule**
 *
 * Imports:
 *
 * [[AuthenticationModule]], [[SubjectModule]], [[FacultyModule]],
 *
 * [[UserModule]], [[AssignmentsModule]], [[AssignmentSolutionModule
 * @packageDocumentation
 * @module UI
 * @preferred
 */

import {Module} from "@nestjs/common";
import {AuthenticationModule} from "./modules/authentication/AuthenticationModule";
import {SubjectModule} from "./modules/subject/SubjectModule";
import {FacultyModule} from "./modules/faculty/FacultyModule";
import {UserModule} from "./modules/user/UserModule";
import {AssignmentsModule} from "./modules/assignments/AssignmentsModule";
import {AssignmentSolutionModule} from "./modules/assignment-solution/AssignmentSolutionModule";

/**
 * NestJS Module: **APIModule**
 *
 * Imports:
 *
 * [[AuthenticationModule]], [[SubjectModule]], [[FacultyModule]],
 *
 * [[UserModule]], [[AssignmentsModule]], [[AssignmentSolutionModule]]
 */
@Module({
    imports: [
        AuthenticationModule,
        SubjectModule,
        FacultyModule,
        UserModule,
        AssignmentsModule,
        AssignmentSolutionModule
    ]
})
export class APIModule {}