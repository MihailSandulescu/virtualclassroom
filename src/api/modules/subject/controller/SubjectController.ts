/**
 * @packageDocumentation
 * @module API/Subject
 */

import {
    BadRequestException,
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    Param,
    Post,
    Put,
    UseGuards,
    UsePipes,
    ValidationPipe
} from "@nestjs/common";
import {ObjectId} from "bson";
import {SubjectService} from "../service/SubjectService";
import {Subject} from "../models/Subject";
import {SubjectDTO} from "../models/SubjectDTO";
import {AuthenticatedGuard} from "../../authentication/models/guards/AuthenticatedGuard";
import {ProfessorGuard} from "../../authentication/models/guards/ProfessorGuard";
import {IPostResponse} from "../../../../utils";


/**
 * Controller for handling all [`/api/Subjects`](/licentavirtualclassroom/docs/swagger/#/Subject) routes
 */
@Controller("api/subjects")
@UseGuards(AuthenticatedGuard)
export class SubjectController {

    constructor(private readonly subjectService: SubjectService) {}

    /**
     * Retrieve all Subjects.
     *
     * Handle [GET `/api/Subjects`](/licentavirtualclassroom/docs/swagger/#/Subject/get_Subjects)
     */
    @Get()
    public getSubjects(): Promise<Subject[]> {
        return this.subjectService.getSubjects();
    }

    /**
     * Retrieve a Subject by ID
     *
     * Handle [GET `/api/Subjects/:id`](/licentavirtualclassroom/docs/swagger/#/Subject/get_Subjects__id_)
     * @param id
     */
    @Get(":id")
    public getSubject(@Param("id") id: string): Promise<Subject> {
        if (!ObjectId.isValid(id)) {
            throw new BadRequestException("The provided ID is not a valid ObjectId.");
        }

        return this.subjectService.getSubject(id);
    }

    /**
     * Add a new Subject
     *
     * Handle [POST `/api/Subjects`](/licentavirtualclassroom/docs/swagger/#/Subject/post_Subjects)
     * @param SubjectDto
     */
    @Post()
    @UseGuards(ProfessorGuard)
    @UsePipes(new ValidationPipe({ whitelist: true }))
    public postSubject(
        @Body() SubjectDto: SubjectDTO,
    ): Promise<IPostResponse> {

        return this.subjectService.addSubject(SubjectDto)
            .then((SubjectId: ObjectId) => {
                return { _id: SubjectId.toHexString() }
            });
    }

    /**
     * Update an existing Subject by ID
     *
     * Handle [PUT `/api/Subjects/:id`](/licentavirtualclassroom/docs/swagger/#/Subject/put_Subjects__id_)
     * @param id
     * @param SubjectDto
     */
    @Put(":id")
    @UseGuards(ProfessorGuard)
    @UsePipes(new ValidationPipe({ skipMissingProperties: true, whitelist: true }))
    public putSubject(
        @Param("id") id: string,
        @Body() SubjectDto: SubjectDTO
    ): Promise<Subject> {
        if (!ObjectId.isValid(id)) {
            throw new BadRequestException("The provided ID is not a valid ObjectId.");
        }

        return this.subjectService.updateSubject(id, SubjectDto);
    }


    /**
     * Delete an Subject by ID
     *
     * Handle [DELETE `/api/Subjects/:id`](/licentavirtualclassroom/docs/swagger/#/Subject/delete_Subjects__id_)
     * @param id
     */
    @Delete(":id")
    @UseGuards(ProfessorGuard)
    @HttpCode(204)
    public deleteSubject(@Param("id") id: string): Promise<void> {
        if (!ObjectId.isValid(id)) {
            throw new BadRequestException("The provided ID is not a valid ObjectId.");
        }

        return this.subjectService.deleteSubject(id);
    }
}

