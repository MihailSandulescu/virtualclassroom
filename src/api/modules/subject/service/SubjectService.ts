/**
 * @packageDocumentation
 * @module API/Subject
 */

import {Inject, Injectable} from "@nestjs/common";
import {Subject} from "../models/Subject";
import {InstanceType} from "@typegoose/typegoose";
import {Model} from "../../mongoose/model";
import {SubjectDTO} from "../models/SubjectDTO";
import {NoResultException} from "../../../exceptions/models/NoResultException";
import {ObjectId} from "mongodb";

/**
 * Service containing business logic for handling Subject operations
 */
@Injectable()
export class SubjectService {
    constructor(
        @Inject("SubjectModel") private readonly SubjectModel: Model<Subject>
    ) {}

    /**
     * Retrieve a Subject by the Subject ID
     * @param id - String representation of an ObjectId
     * @returns Promise<ObjectId>
     */
    public async getSubject(id: string): Promise<Subject> {
        const subject: InstanceType<Subject> | null = await this.SubjectModel.findById(id)
            .populate({ path: "professor", model: "User" });

        if (!subject) {
            throw new NoResultException(`Subject ${id} does not exist.`);
        }

        return subject;
    };

    /**
     * Retrieve a Subject given the Name
     * @returns Promise<ObjectId>
     * @param identifier - name
     */
    public async getSubjectByName(identifier: string): Promise<Subject> {
        const subject: Subject = await this.SubjectModel.findOne({
            name: identifier
        });

        if (!subject) {
            throw new NoResultException(`Subject with identifier ${identifier} does not exist.`);
        }

        return subject;
    };

    /**
     * Retrieves all Subjects
     * @returns Promise<Array<[[Subject]]>>
     */
    public async getSubjects(): Promise<Subject[]> {
        const subject: InstanceType<Subject[]> = await this.SubjectModel.find({})
            .populate({ path: "professor", model: "User" });

        return subject;
    };

    /**
     * Add a new Subject in the database
     * @param payload and ISubject object representing the Subject that will be loaded
     * @returns Promise<ObjectId> the _id field of the inserted Subject
     */
    public async addSubject(payload: SubjectDTO): Promise<ObjectId> {
        const newSubject: Subject = await new this.SubjectModel(payload).save();

        return newSubject._id;
    }

    /**
     * Update an Subject
     * @param id - the _id of the Subject to update
     * @param payload - the data transfer object received from the front-end
     * @return result - the modified Subject
     */
    public async updateSubject(id: string, payload: SubjectDTO): Promise<Subject> {
        const result: InstanceType<Subject> | null = await this.SubjectModel
            .findByIdAndUpdate(id, payload, { new: true });

        if (result === null) {
            throw new NoResultException(`Subject ID ${id} does not exist.`);
        }

        return result;
    }

    /**
     * Delete a Subject from the database based on an ID
     * @param id - Subject's ID
     * @return Promise<void>
     */
    public async deleteSubject(id: string): Promise<void> {
        await this.SubjectModel.findByIdAndRemove(id);
    }
}
