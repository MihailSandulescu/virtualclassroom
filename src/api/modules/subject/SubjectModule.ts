/**
 * NestJS Module: **Subject**
 *
 * Controller: [[SubjectController]]
 *
 * Exports: [[SubjectService]]
 *
 * Provides: [[SubjectService]], [[SubjectModel]]
 *
 * @packageDocumentation
 * @module API/Subject
 * @preferred
 */

import { Module } from '@nestjs/common';
import {SubjectService} from "./service/SubjectService";
import {SubjectModel} from "./models/SubjectModel";
import {SubjectController} from "./controller/SubjectController";

/**
 * NestJS Module: **Subject**
 *
 * Controller: [[SubjectController]]
 *
 * Exports: [[SubjectService]]
 *
 * Provides: [[SubjectService]], [[SubjectModel]]
 *
 */
@Module({
    controllers: [SubjectController],
    providers: [
        SubjectService,
        { provide: "SubjectModel", useValue: SubjectModel },
    ],
    exports: [
        SubjectService,
        { provide: "SubjectModel", useValue: SubjectModel },
    ],
})
export class SubjectModule {}
