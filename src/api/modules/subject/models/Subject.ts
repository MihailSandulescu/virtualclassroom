/**
 * @packageDocumentation
 * @module API/Subjects
 */
import {ObjectId} from "bson";
import {instanceMethod, prop, Typegoose} from "@typegoose/typegoose";
import {ISubject, ISubjectTimetable} from "./ISubject";
import {User} from "../../user/models/User";
import {Assignment} from "../../assignments/models/Assignment";
import parser, {CronExpression} from "cron-parser";

export class Subject extends Typegoose implements ISubject {
    public _id: ObjectId;

    @prop({ required: true })
    public name: string;

    @prop({ required: true })
    public year: number;

    @prop({ required: true })
    public timetable: ISubjectTimetable;

    @prop({ required: true })
    public semester: string;

    @prop()
    public credits?: number;

    @prop({ required: true })
    public professor: string | ObjectId | User;

    @prop({ required: true })
    public assignments: (string | ObjectId | Assignment)[];

    public createdAt: Date;

    public updatedAt: Date;

    /**
     * Retrieve a list of dates representing the timetable of this subject
     * @param startDate - optional start date for the timetable received
     * @param endDate - optional end date for the timetable received
     */
    @instanceMethod
    public getTimetable(this: Subject, startDate?: Date, endDate?: Date): Date[] {
        const subjectStartTime: Date = new Date(this.timetable.startDate);
        const subjectEndTime: Date = new Date(this.timetable.endDate);
        const start: Date = startDate && startDate.getTime() > subjectStartTime.getTime() ? startDate : subjectStartTime;
        const end: Date = endDate && endDate.getTime() < subjectEndTime.getTime() ? endDate : subjectEndTime;

        const interval: CronExpression<true> = parser.parseExpression(this.timetable.cron, {
            currentDate: start,
            endDate: end,
            iterator: true
        });

        const dates: Date[] = [];

        while (interval.hasNext()) {
            dates.push(interval.next().value.toDate());
        }

        return dates;
    }
}
