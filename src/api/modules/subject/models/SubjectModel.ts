/**
 * @packageDocumentation
 * @module API/Subjects
 */

import MongooseService from "../../mongoose/service/MongooseService";
import {Model} from "../../mongoose/model";
import {Subject} from "./Subject";

export const SubjectModel: Model<Subject> = new Subject().getModelForClass(Subject, {
    existingConnection: MongooseService.getInstance().getClient(),
    schemaOptions: { collection: 'Subjects', versionKey: false, timestamps: true}
});
