/**
 * @packageDocumentation
 * @module API/Subject
 */

import {ObjectId} from "bson";
import {User} from "../../user/models/User";
import {Assignment} from "../../assignments/models/Assignment";

export interface ISubjectTimetable {
    startDate: string;
    endDate: string;
    cron: string;
}

export interface ISubject {
    name: string;
    year: number;
    timetable: ISubjectTimetable;
    semester: string;
    credits?: number;
    professor: string | ObjectId | User;
    assignments: (string | ObjectId | Assignment)[];
}