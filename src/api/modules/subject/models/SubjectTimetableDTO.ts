/**
 * @packageDocumentation
 * @module API/Subjects
 */

import {ISubjectTimetable} from "./ISubject";
import {IsISO8601} from "class-validator";
import {IsCronExpression} from "../../../decorators/IsCronExpression";

export class SubjectTimetableDTO implements ISubjectTimetable {
    @IsISO8601()
    public startDate: string;

    @IsISO8601()
    public endDate: string;

    @IsCronExpression()
    public cron: string;
}