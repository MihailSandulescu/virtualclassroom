/**
 * @packageDocumentation
 * @module API/Subjects
 */

import {ISubject, ISubjectTimetable} from "./ISubject";
import {IsNumber, IsOptional, IsString, ValidateNested} from "class-validator";
import {Type} from "class-transformer";
import {SubjectTimetableDTO} from "./SubjectTimetableDTO";

export class SubjectDTO implements ISubject {
    @IsString()
    public name: string;

    @IsNumber()
    public year: number;

    @ValidateNested()
    @Type(() => SubjectTimetableDTO)
    public timetable: ISubjectTimetable;

    @IsString()
    public semester: string;

    @IsOptional()
    @IsNumber()
    public credits?: number;

    @IsString()
    public professor: string;

    @IsString({ each: true })
    public assignments: string[];
}