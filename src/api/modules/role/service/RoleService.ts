/**
 * @packageDocumentation
 * @module API/Role
 */


import {AccessRole, IRole, Role} from "../models/Role";
import {ObjectId} from "bson";
import {Inject, Injectable} from "@nestjs/common";
import {Model} from "../../mongoose/model";
import {InstanceType} from "@typegoose/typegoose";
import {NoResultException} from "../../../exceptions/models/NoResultException";

/**
 * Service class for manipulating [[Role]]s
 */
@Injectable()
export class RoleService {

    constructor(
        @Inject("RoleModel") private readonly roleModel: Model<Role>
    ) {}

    /**
     * Retrieve the role for a given user ID
     * @param userId - the [[User]]'s ID
     * @returns Promise<[[Role]]>
     */
    public async getRole(userId: string): Promise<Role> {
        const role: Role = await this.roleModel.findOne({
            userId: ObjectId.createFromHexString(userId)
        });

        if (!role) {
            throw new NoResultException(`User ${userId} has no roles.`);
        }

        return role;
    };

    /**
     * Update a role for a given user ID
     * @param userId - the [[User]]'s ID
     * @param roleLevel - the role to give the user
     * @returns Promise<[[Role]]>
     */
    public async updateRole(userId: ObjectId, roleLevel: AccessRole): Promise<Role> {
        const role: InstanceType<Role> = await this.roleModel.findOneAndUpdate({ userId }, {
            $set: { roleLevel }
        }, {
            new: true,
            runValidators: true
        });

        if (role === null) {
            throw new NoResultException(`User ${userId} has no roles.`);
        }

        return role;
    };

    /**
     * Add a new role
     * @param payload - the [[Role]] object
     * @returns Promise<ObjectId>
     */
    public async addRole(payload: IRole): Promise<Role> {
        return new this.roleModel(payload).save();
    };

    /**
     * Delete an existing role
     * @param id - the role id
     */
    public async deleteRole(id: string): Promise<void> {
        const role: InstanceType<Role> | null = this.roleModel.findByIdAndRemove(id);
        if (!role) throw new NoResultException(`Role id ${id} does not exist.`);
    }
}
