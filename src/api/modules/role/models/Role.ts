/**
 * @module API/Role
 */

/**
 * Available access roles
 */
import {ObjectId} from "bson";
import {prop, Typegoose} from "@typegoose/typegoose";

export enum AccessRole {
    PowerlessRole = "GUEST",
    StudentRole = "STUDENT",
    ProfessorRole = "PROFESSOR",
    AdminRole = "ADMIN"
}

export interface IRole {
    userId: string | ObjectId;
    roleLevel: AccessRole;
}

/**
 * Object mapping of a Role document
 */
export class Role extends Typegoose implements IRole {

    public _id: ObjectId;

    /**
     * [Required] The id of the user associated with this role
     */
    @prop({ required: true })
    public userId: ObjectId;

    /**
     * [Required] The assigned role. Defaults to `[[AccessRole]].StudentRole`
     */
    @prop({ required: true, default: AccessRole.StudentRole })
    public roleLevel: AccessRole;
}