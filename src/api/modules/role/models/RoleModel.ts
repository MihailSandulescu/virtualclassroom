/**
 * @module API/Role
 */

/**
 * Typegoose model for database interaction
 */
import {Model} from "../../mongoose/model";
import {Role} from "./Role";
import MongooseService from "../../mongoose/service/MongooseService";

export const RoleModel: Model<Role> = new Role().getModelForClass(Role, {
    existingConnection: MongooseService.getInstance().getClient(),
    schemaOptions: { collection: "Roles", versionKey: false }
});
