/**
 * NestJS Module: **Role**
 *
 * Controller: [[]]
 *
 * Exports: [[RoleService]]
 *
 * Provides: [[RoleService]], [[RoleModel]]
 * @packageDocumentation
 * @module API/Role
 * @preferred
 */

import {Module} from "@nestjs/common";
import {RoleService} from "./service/RoleService";
import {RoleModel} from "./models/RoleModel";

/**
 * NestJS Module: **Role**
 *
 * Controller: [[]]
 *
 * Exports: [[RoleService]]
 *
 * Provides: [[RoleService]], [[RoleModel]]
 */
@Module({
    controllers: [],
    providers: [
        RoleService,
        { provide: "RoleModel", useValue: RoleModel },
    ],
    exports: [
        RoleService
    ]
})
export class RoleModule {}