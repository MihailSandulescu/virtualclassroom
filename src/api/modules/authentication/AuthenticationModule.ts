/**
 * NestJS Module: **Authentication**
 *
 * Controller: [[AuthenticationController]]
 *
 * Exports: [[AuthenticationService]], [[JWTGenerationService]]
 *
 * Provides: [[AuthenticationService]], [[JWTGenerationService]]
 * @packageDocumentation
 * @module API/Authentication
 * @preferred
 */

import {Module} from '@nestjs/common';
import {AuthenticationService} from "./service/AuthenticationService";
import JWTGenerationService from "./service/JWTGenerationService";
import {UserModule} from "../user/UserModule";
import {AuthenticationController} from "./controller/AuthenticationController";
import {JwtStrategy} from "./models/passport/AuthenticationStrategy";

/**
 * NestJS Module: **Authentication**
 *
 * Controller: [[AuthenticationController]]
 *
 * Exports: [[AuthenticationService]], [[JWTGenerationService]]
 *
 * Provides: [[AuthenticationService]], [[JWTGenerationService]]
 *
 * Imports: [[UserModule]]
 */
@Module({
    controllers: [AuthenticationController],
    providers: [
        AuthenticationService,
        JWTGenerationService,
        JwtStrategy
    ],
    exports: [
        AuthenticationService,
        JWTGenerationService,
    ],
    imports: [UserModule]
})
export class AuthenticationModule {}
