/**
 * @packageDocumentation
 * @module API/Authentication
 */

import {Injectable} from "@nestjs/common";
import {UserService} from "../../user/service/UserService";
import {User} from "../../user/models/User";
import JWTGenerationService from "./JWTGenerationService";
import bcrypt from "bcrypt";
import {AuthenticationException} from "../../../exceptions/models/AuthenticationException";


/**
 * Service class for authenticating users
 */
@Injectable()
export class AuthenticationService {

    constructor(
        private readonly userService: UserService,
        private readonly jwtService: JWTGenerationService
    ) {}

    /**
     * Authenticate with an identifier and a password
     * @param identification - email or phone number
     * @param password - the password
     * @throws ValidationError
     * @returns JWT
     */
    public async authenticate(identification: string, password: string): Promise<string> {
        const user: User = await this.userService.getUserByEmailOrPhone(identification);
        const isPasswordCorrect: boolean = await bcrypt.compare(password, user.password);

        if (!isPasswordCorrect) {
            throw new AuthenticationException("Wrong identifier or password");
        }

        return this.jwtService.generate(user._id.toHexString());
    };
}