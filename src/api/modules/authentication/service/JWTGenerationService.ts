/**
 * @packageDocumentation
 * @module API/Authentication
 */

import jwt from "jsonwebtoken";
import fs from "fs";
import path from "path";
import {promisify} from "util";
import {Injectable} from "@nestjs/common";
import {Configuration, IAuthConfig} from "../../../../utils/Configuration";
import {PromisifiedReadFile, PromisifiedSign} from "../models";

/**
 * Promisified version of fs.readFile
 */
export const readFile: PromisifiedReadFile = promisify<fs.PathLike | number, { encoding?: string | null; flag?: string; } | string | undefined | null, string | Buffer>(fs.readFile);
/**
 * Promisified version of jwt.sign
 */
export const sign: PromisifiedSign = promisify<string | Buffer | object, jwt.Secret, jwt.SignOptions, string>(jwt.sign);

/**
 * Service class for generating JWT
 */
@Injectable()
class JWTGenerationService {

    constructor() {}


    /**
     * Generate new JWT for the provided subject
     * @param subject - the [[User]]'s _id
     * @returns Promise<string>
     */
    public async generate(subject: string): Promise<string> {
        const config: IAuthConfig = Configuration.getInstance().getAuthConfig();

        const options: jwt.SignOptions = {
            algorithm: config.alg,
            expiresIn: config.expiration,
            notBefore: config.notBefore,
            audience: config.audience,
            issuer: config.issuer,
            subject: subject
        };

        const cert: string | Buffer = await readFile(path.resolve(__dirname, "../../../../", config.privKeyPath), { encoding: "utf8" })
        return sign({}, cert, options);
    }
}

export default JWTGenerationService;
