/**
 * @packageDocumentation
 * @module API/Authentication
 */

import {Body, Controller, HttpCode, Post, UsePipes, ValidationPipe} from "@nestjs/common";
import {AuthenticationService} from "../service/AuthenticationService";
import {LoginDTO} from "../models/LoginDTO";

export interface IAuthenticationResponse {
    token: string;
}

/**
 * Controller for handling all [`/api`](/licentavirtualclassroom/docs/swagger/#/Authentication) routes
 */
@Controller("api/auth")
export class AuthenticationController {

    constructor(
        private readonly authenticationService: AuthenticationService
    ) {}

    /**
     * Authenticate with credentials and receive a JWT
     *
     * Handle [POST `/api/auth/login`](/licentavirtualclassroom/docs/swagger/#/Authentication/post_auth_login)
     */
    @Post("login")
    @HttpCode(200)
    @UsePipes(new ValidationPipe({ whitelist: true }))
    public async login(
        @Body() loginDto: LoginDTO
    ): Promise<IAuthenticationResponse> {
        const jwt: string = await this.authenticationService.authenticate(
            loginDto.identifier, loginDto.password
        );

        return { token: jwt };
    }
}

