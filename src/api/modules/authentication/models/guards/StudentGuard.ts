/**
 * @packageDocumentation
 * @module API/Authentication
 */

import {CanActivate, ExecutionContext, Injectable} from "@nestjs/common";
import {Observable} from "rxjs";
import {User} from "../../../user/models/User";
import {AccessRole, Role} from "../../../role/models/Role";

@Injectable()
export class StudentGuard implements CanActivate {
    /**
     * Checks whether this guard is applicable
     * @param context
     */
    public canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
        const req: Express.Request = context.switchToHttp().getRequest();

        const user: User | undefined = req.user as User | undefined;
        if (!user) return false;

        const role: Role | undefined = user.role as Role | undefined;
        if (!role) return false;

        return role.roleLevel === AccessRole.StudentRole ||
            role.roleLevel === AccessRole.ProfessorRole ||
            role.roleLevel === AccessRole.AdminRole;
    }
}