/**
 * @packageDocumentation
 * @module API/Authentication
 */

import {Injectable} from "@nestjs/common";
import {AuthGuard} from "@nestjs/passport";

/**
 * This guard will work just as the [[AuthenticatedGuard]], however it will not
 * throw an error if the authentication fails. This is done so that endpoints using this
 * guard are able to verify existing incoming credentials, but not error when these credentials are missing
 */
@Injectable()
export class OptionalAuthGuard extends AuthGuard('jwt') {
    /**
     * Overriden method that handles the request incoming to the JWT AuthGuard.
     * @param err
     * @param user
     */
    public handleRequest<User>(err: Error, user: User): User {
        return user;
    }
}
