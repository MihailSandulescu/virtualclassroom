/**
 * @packageDocumentation
 * @module API/Authentication
 */

import {ExtractJwt} from "passport-jwt";
import {Request} from "express";

/**
 * This function extends the ExtractJwt.fromAuthHeaderAsBearerToken by also looking up the JWT
 * inside a given cookie field if the Authentication header
 * @param field - the cookie name
 * @constructor
 */
export function ExtractJwtAuthHeaderOrCookie(field: string): (request: Request) => string | null {
    return function (request: Request): string | null {
        const extractedFromAuthHeader: string | null = ExtractJwt.fromAuthHeaderAsBearerToken()(request);
        if (extractedFromAuthHeader) return extractedFromAuthHeader;

        const extractedFromCookie: string | undefined = request.cookies[field];

        return extractedFromCookie || null;
    }
}