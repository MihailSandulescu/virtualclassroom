/**
 * @packageDocumentation
 * @module API/Authentication
 */

import {PassportStrategy} from "@nestjs/passport";
import {Strategy} from "passport-jwt";
import {Configuration, IAuthConfig} from "../../../../../utils/Configuration";
import path from "path";
import fs from "fs";
import {UserService} from "../../../user/service/UserService";
import {Injectable, UnauthorizedException} from "@nestjs/common";
import {User} from "../../../user/models/User";
import {ExtractJwtAuthHeaderOrCookie} from "./ExtractJwtAuthHeaderOrCookie";

export interface ITokenBody {
    iat: number;
    nbf: number;
    exp: number;
    aud: string;
    iss: string;
    sub: string;
}

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    private readonly userService: UserService;

    constructor(userService: UserService) {
        const config: IAuthConfig = Configuration.getInstance().getAuthConfig();

        const pubKey: string = fs.readFileSync(path.resolve(__dirname, "../../../../../", config.pubKeyPath), { encoding: "utf8" })

        super({
            jwtFromRequest: ExtractJwtAuthHeaderOrCookie('session_token'),
            secretOrKey: pubKey,
            issuer: config.issuer,
            audience: config.audience,
            algorithms: [config.alg]
        });

        this.userService = userService;
    }

    /**
     * Validates whether the payload is for a valid user
     * @param payload
     */
    public async validate(payload: ITokenBody): Promise<User> {
        try {
            const user: User = await this.userService.getUser(payload.sub);
            return user;
        } catch (e) {
            throw new UnauthorizedException(e.message);
        }
    }

}