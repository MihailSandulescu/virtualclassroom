/**
 * @packageDocumentation
 * @module API/Authentication
 */

import {IsString} from "class-validator";

export class LoginDTO {
    @IsString()
    public identifier: string;

    @IsString()
    public password: string;
}