import fs from "fs";

type PromisifiedSign = (payload: string | Buffer | object, secretOrPrivateKey: Secret, options: SignOptions) => Promise<string>;
type PromisifiedVerify = (token: string, secretOrPublicKey: string | Buffer, options: VerifyOptions) => Promise<object | string>;
type PromisifiedReadFile = (path: fs.PathLike | number, options: { encoding?: string | null; flag?: string; } | string | undefined | null) => Promise<string | Buffer>;
