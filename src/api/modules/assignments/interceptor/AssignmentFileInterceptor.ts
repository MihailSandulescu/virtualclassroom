/**
 * @packageDocumentation
 * @module API/Assignments
 */
import {CallHandler, ExecutionContext, Inject, mixin, NestInterceptor, Optional, Type,} from '@nestjs/common';
import multer from 'multer';
import {Observable} from 'rxjs';
import {MULTER_MODULE_OPTIONS} from "@nestjs/platform-express/multer/files.constants";
import {MulterModuleOptions} from "@nestjs/platform-express";
import {MulterOptions} from "@nestjs/platform-express/multer/interfaces/multer-options.interface";
import {transformException} from "@nestjs/platform-express/multer/multer/multer.utils";
import {Request, Response} from "express";
import {User} from "../../user/models/User";
import {multerBuilder} from "../../../middleware/multer";
import {ObjectId} from "bson";

type MulterInstance = any;

export function AssignmentFileInterceptor(
    fieldName: string,
    localOptions?: MulterOptions,
): Type<NestInterceptor> {
    class MixinInterceptor implements NestInterceptor {
        protected multer: MulterInstance;

        constructor(
            @Optional()
            @Inject(MULTER_MODULE_OPTIONS)
                options: MulterModuleOptions = {},
        ) {
            this.multer = (multer as any)({
                ...options,
                ...localOptions,
            });
        }

        /**
         * Intercept file uploads and use multer settings
         * @param context
         * @param next
         */
        public async intercept(
            context: ExecutionContext,
            next: CallHandler,
        ): Promise<Observable<any>> {
            const req: Request = context.switchToHttp().getRequest();
            const res: Response = context.switchToHttp().getResponse();
            const user: User = req.user as User;
            const fileTypes: string[] = [
                "application/zip",
                "application/x-7z-compressed",
                "application/gzip",
                "application/x-bzip2",
                "application/x-bzip",
                "application/vnd.rar",
                "application/x-freearc",
                "application/java-archive",
                "application/x-tar",
                "application/x-zip-compressed"
            ];
            const multerOpts: multer.Options = multerBuilder(
                fileTypes,
                `${user._id.toHexString()}/${ObjectId.createFromTime(new Date().getTime()).toHexString()}`
            );

            await new Promise((resolve, reject) => multer(multerOpts).single(fieldName)(
                req, res, (err: any) => {
                    if (err) {
                        const error: Error = transformException(err);
                        return reject(error);
                    }
                    resolve();
                }
            ));

            return next.handle();
        }
    }

    const Interceptor: Type<MixinInterceptor> = mixin(MixinInterceptor);
    return Interceptor as Type<NestInterceptor>;
}