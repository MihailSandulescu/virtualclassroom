/**
 * @packageDocumentation
 * @module API/Assignments
 */
import {
    Body,
    Controller,
    Delete,
    Get,
    Header,
    Param,
    Post,
    Put,
    Req,
    Res,
    UploadedFile,
    UseGuards,
    UseInterceptors,
    UsePipes,
    ValidationPipe
} from "@nestjs/common";
import {AssignmentsService} from "../service/AssignmentsService";
import {StudentGuard} from "../../authentication/models/guards/StudentGuard";
import {Assignment} from "../models/Assignment";
import {AuthenticatedGuard} from "../../authentication/models/guards/AuthenticatedGuard";
import {User} from "../../user/models/User";
import path from "path";
import {AssignmentDTO} from "../models/dto/AssignmentDTO";
import {AssignmentFileInterceptor} from "../interceptor/AssignmentFileInterceptor";
import {ProfessorGuard} from "../../authentication/models/guards/ProfessorGuard";
import {Response} from "express";

@Controller("api/assignments")
@UseGuards(AuthenticatedGuard, StudentGuard)
export class AssignmentsController {

    constructor(
        private readonly assignmentsService: AssignmentsService
    ) {}

    /**
     * List all assignments in the database
     */
    @Get()
    public async getAssignments(): Promise<Assignment[]> {
        return this.assignmentsService.getAssignments();
    }

    /**
     * List the current user's assignments
     */
    @Get("my")
    public async getMyAssignments(@Req() req: Express.Request): Promise<Assignment[]> {
        const user: User = req.user as User;
        return this.assignmentsService.getAssignmentsForUser(user._id.toHexString());
    }

    /**
     * List all assignments for a given user id
     * @param userId - the user id
     */
    @Get("user/:id")
    public async getAssignmentsForUser(@Param("id") userId: string): Promise<Assignment[]> {
        return this.assignmentsService.getAssignmentsForUser(userId);
    }

    /**
     * List all assignments for a given groupId
     * @param groupId - the group id
     */
    @Get("group/:id")
    public async getAssignmentsForGroup(@Param("id") groupId: string): Promise<Assignment[]> {
        return this.assignmentsService.getAssignmentsForGroup(groupId);
    }

    /**
     * Retrieve the details of an assignment
     * @param id
     */
    @Get(":id")
    public async getAssignment(@Param("id") id: string): Promise<Assignment> {
        return this.assignmentsService.getAssignment(id);
    }

    /**
     * Download an assignment upload
     * @param id - the assignment upload
     * @param res - Express.Response object
     */
    @Get(":id/download")
    @Header("Content-Type", "application/octet-stream")
    public async getAssignmentUpload(
        @Param("id") id: string,
        @Res() res: Response
    ): Promise<void> {
        const assignment: Assignment = await this.assignmentsService.getAssignment(id);

        return res.download(path.resolve(__dirname, "../../../../", assignment.file));
    }

    /**
     * Adds a new assignment. Only Professors can do that.
     * @param assignmentDto - the assignment object
     * @param file - attachments
     */
    @Post("")
    @UseGuards(ProfessorGuard)
    @UseInterceptors(AssignmentFileInterceptor("file"))
    @UsePipes(new ValidationPipe({ whitelist: true }))
    public async postAssignment(
        @Body() assignmentDto: AssignmentDTO,
        @UploadedFile() file: Express.Multer.File
    ): Promise<Assignment> {
        let segments: string[] = file.destination.split("/");
        segments = segments.slice(Math.max(segments.length - 3, 0));
        assignmentDto.file = path.join(...segments, file.filename);

        return this.assignmentsService.addAssignment(assignmentDto);
    }

    /**
     * Modify an existing assignment. Only Professors can do that.
     * @param id - the assignment id
     * @param assignmentDto - the new assignment object
     * @param file - attachments
     */
    @Put(":id")
    @UseGuards(ProfessorGuard)
    @UseInterceptors(AssignmentFileInterceptor("file"))
    @UsePipes(new ValidationPipe({ skipMissingProperties: true, whitelist: true }))
    public async putAssignment(
        @Param("id") id: string,
        @Body() assignmentDto: AssignmentDTO,
        @UploadedFile() file: Express.Multer.File
    ): Promise<Assignment> {
        let segments: string[] = file.destination.split("/");
        segments = segments.slice(Math.max(segments.length - 3, 0));
        assignmentDto.file = path.join(...segments, file.filename);

        return this.assignmentsService.updateAssignment(id, assignmentDto);
    }

    /**
     * Delete an existing assignment. Only Professors can do that.
     * @param id - the assignment id
     */
    @Delete(":id")
    @UseGuards(ProfessorGuard)
    public async deleteAssignment(@Param("id") id: string): Promise<Assignment> {
        return this.assignmentsService.deleteAssignment(id);
    }

}