/**
 * NestJS Module: **Assignments**
 *
 * Controller: [[AssignmentsController]]
 *
 * Exports: [[AssignmentsService]]
 *
 * Imports: [[UserService]], [[SubjectModule]]
 *
 * Provides: [[AssignmentsService]], [[AssignmentModel]]
 * @packageDocumentation
 * @module API/Assignments
 * @preferred
 */

import {Module} from "@nestjs/common";
import {AssignmentModel} from "./models/Assignment";
import {AssignmentsController} from "./controller/AssignmentsController";
import {AssignmentsService} from "./service/AssignmentsService";
import {UserModule} from "../user/UserModule";
import {SubjectModule} from "../subject/SubjectModule";

/**
 * NestJS Module: **Assignments**
 *
 * Controller: [[AssignmentsController]]
 *
 * Imports: [[UserService]], [[SubjectModule]]
 *
 * Exports: [[AssignmentsService]]
 *
 * Provides: [[AssignmentsService]], [[AssignmentModel]]
 */
@Module({
    controllers: [AssignmentsController],
    imports: [UserModule, SubjectModule],
    exports: [
        AssignmentsService
    ],
    providers: [
        AssignmentsService,
        { provide: "AssignmentModel", useValue: AssignmentModel },
    ]
})
export class AssignmentsModule {}