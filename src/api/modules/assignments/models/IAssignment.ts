/**
 * @packageDocumentation
 * @module API/Assignments
 */
import {ObjectId} from "bson";
import {Group} from "../../group/models/Group";
import {User} from "../../user/models/User";
import {Subject} from "../../subject/models/Subject";

export interface IAssignment {
    name: string;
    description: string;
    file?: string;
    deadline: Date;
    subject: ObjectId | Subject | string;
    groups: (ObjectId | Group | string)[];
    students: (ObjectId | User | string)[];
}