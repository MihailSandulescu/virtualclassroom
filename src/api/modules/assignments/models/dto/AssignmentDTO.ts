/**
 * @packageDocumentation
 * @module API/Assignments
 */
import {IAssignment} from "../IAssignment";
import {IsISO8601, IsMongoId, IsString, ValidateIf} from "class-validator";
import {Transform} from "class-transformer";

export class AssignmentDTO implements IAssignment {
    @IsString()
    public name: string;

    @IsString()
    public description: string;

    public file: string;

    @IsISO8601()
    public deadline: Date;

    @IsMongoId()
    public subject: string;

    @ValidateIf((assignment: IAssignment) => !assignment.students)
    @IsMongoId({ each: true })
    @Transform((groups: string[] | string) => {
        if (Array.isArray(groups)) return groups;
        else if (groups) return [groups];
        else return [];
    })
    public groups: string[];

    @ValidateIf((assignment: IAssignment) => !assignment.groups)
    @IsMongoId({ each: true })
    @Transform((students: string[] | string) => {
        if (Array.isArray(students)) return students;
        else if (students) return [students];
        else return [];
    })
    public students: string[];
}