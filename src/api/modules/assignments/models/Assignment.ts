/**
 * @packageDocumentation
 * @module API/Assignments
 */
import {InstanceType, prop, Typegoose} from "@typegoose/typegoose";
import {ObjectId} from "bson";
import {IAssignment} from "./IAssignment";
import mongoose from "mongoose";
import MongooseService from "../../mongoose/service/MongooseService";
import {User} from "../../user/models/User";
import {Group} from "../../group/models/Group";
import {Subject} from "../../subject/models/Subject";

export class Assignment extends Typegoose implements IAssignment {
    public _id: ObjectId;

    @prop({ required: true })
    public name: string;

    @prop({ required: true })
    public description: string;

    @prop({ required: true })
    public file: string;

    @prop({ required: true })
    public deadline: Date;

    @prop({ required: true, index: true })
    public subject: ObjectId | Subject | string;

    @prop({ default: [], index: true })
    public groups: (ObjectId | Group | string)[];

    @prop({ default: [], index: true })
    public students: (ObjectId | User | string)[];

    public createdAt: Date;

    public updatedAt: Date;
}

export const AssignmentModel: mongoose.Model<InstanceType<Assignment>> = new Assignment().getModelForClass(Assignment, {
    existingConnection: MongooseService.getInstance().getClient(),
    schemaOptions: { collection: 'Assignment', versionKey: false, timestamps: true}
});