/**
 * @packageDocumentation
 * @module API/Assignments
 */
import {Inject, Injectable, NotFoundException} from "@nestjs/common";
import mongoose from "mongoose";
import {InstanceType} from "@typegoose/typegoose";
import {Assignment} from "../models/Assignment";
import {NoResultException} from "../../../exceptions/models/NoResultException";
import {ObjectId} from "bson";
import {AssignmentDTO} from "../models/dto/AssignmentDTO";
import {UserService} from "../../user/service/UserService";
import {User} from "../../user/models/User";
import fs from "fs";
import path from "path";
import {Group} from "../../group/models/Group";
import {Subject} from "../../subject/models/Subject";

@Injectable()
export class AssignmentsService {

    constructor(
        @Inject("AssignmentModel") private readonly assignmentModel: mongoose.Model<InstanceType<Assignment>>,
        @Inject("SubjectModel") private readonly subjectModel: mongoose.Model<InstanceType<Subject>>,
        private readonly userService: UserService
    ) {}

    /**
     * Retrieve all existing assignments
     */
    public async getAssignments(): Promise<Assignment[]> {
        return this.assignmentModel.find({});
    }

    /**
     * Retrieve an assignment by id
     * @param id - the assignment id
     */
    public async getAssignment(id: string): Promise<Assignment> {
        const assignment: Assignment | null = await this.assignmentModel.findById(id);
        if (!assignment) throw new NoResultException(`Assignment id ${id} does not exist`);
        return assignment;
    }

    /**
     * Retrieve all assignment belonging to a user by user id
     * @param userId - the user id
     */
    public async getAssignmentsForUser(userId: string): Promise<Assignment[]> {
        const user: User = await this.userService.getUser(userId);

        return this.assignmentModel.find({
            $or: [
                { students: userId },
                { groups: user.group as Group }
            ]
        }).populate({ path: "subject", model: "Subject" });
    }

    /**
     * Retrieve all assignments belonging to a group by group id
     * @param groupId - the group id
     */
    public async getAssignmentsForGroup(groupId: string): Promise<Assignment[]> {
        return this.assignmentModel.find({
            groups: groupId
        });
    }

    /**
     * Retrieve all assignments belonging to a subject by subject id
     * @param subjectId - the group id
     */
    public async getAssignmentsForSubject(subjectId: string): Promise<Assignment[]> {
        return this.assignmentModel.find({
            subject: subjectId
        });
    }

    /**
     * Retrieve all assignments belonging to a professor
     * @param profId - the professor's id
     */
    public async getAssignmentsForProfessor(profId: string): Promise<Assignment[]> {
        const profSubjects: Subject[] = await this.subjectModel.find({ professor: profId });
        const assignments: Assignment[] = await this.assignmentModel.find({
            subject: { $in: profSubjects.map((s: Subject) => s._id.toHexString()) }
        }).populate({ path: "subject", model: "Subject" });

        return assignments;
    }

    /**
     * Create a new assignment
     * @param assignment - the assignment object
     */
    public async addAssignment(assignment: AssignmentDTO): Promise<Assignment> {
        const model: InstanceType<Assignment> = new this.assignmentModel(assignment);
        const newAssignment: Assignment = await model.save();
        await this.subjectModel.updateOne({ _id: assignment.subject }, { $push: { assignments: newAssignment._id } });

        return newAssignment;
    }

    /**
     * Update an existing assignment
     * @param id - the assignment id
     * @param assignmentDto - the assignment object
     */
    public async updateAssignment(id: string, assignmentDto: AssignmentDTO): Promise<Assignment> {
        const assignment: InstanceType<Assignment> | null = await this.assignmentModel.findByIdAndUpdate(id, {
            $set: {
                ...assignmentDto,
                students: assignmentDto.students.map(sId => ObjectId.createFromHexString(sId)),
                groups: assignmentDto.groups.map(gId => ObjectId.createFromHexString(gId))
            }
        }, { new: true });

        if (!assignment) throw new NoResultException(`Assignment id ${id} does not exist`);

        return assignment;
    }

    /**
     * Delete an existing assignment
     * @param assignmentId - the assignment id
     */
    public async deleteAssignment(assignmentId: string): Promise<Assignment> {
        const deletedAssignment: Assignment | null = await this.assignmentModel.findByIdAndRemove(assignmentId);
        if (!deletedAssignment) throw new NotFoundException(`Assignment id ${assignmentId} does not exist`);

        fs.unlinkSync(path.resolve(__dirname, "../../../../", deletedAssignment.file));

        return deletedAssignment;
    }
}