/**
 * @packageDocumentation
 * @module API/Group
 */

import {
    BadRequestException,
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    Param,
    Post,
    Put,
    UseGuards,
    UsePipes,
    ValidationPipe
} from "@nestjs/common";
import {ObjectId} from "bson";
import {Group} from "../models/Group";
import {GroupDTO} from "../models/GroupDTO";
import {AuthenticatedGuard} from "../../authentication/models/guards/AuthenticatedGuard";
import {IPostResponse} from "../../../../utils";
import {GroupService} from "../service/GroupService";
import {ProfessorGuard} from "../../authentication/models/guards/ProfessorGuard";


/**
 * Controller for handling all [`/api/Group`](/licentavirtualclassroom/docs/swagger/#/Group) routes
 */
@Controller("api/group")
@UseGuards(AuthenticatedGuard)
export class GroupController {

    constructor(private readonly groupService: GroupService) {}

    /**
     * Retrieve all Groups.
     *
     * Handle [GET `/api/Group/all`](/licentavirtualclassroom/docs/swagger/#/Group/all/get_Group)
     */
    @Get("all")
    public getGroups(): Promise<Group[]> {
        return this.groupService.getGroups();
    }

    /**
     * Retrieve a Group by ID
     *
     * Handle [GET `/api/Group/:id`](/licentavirtualclassroom/docs/swagger/#/Group/get_Group__id_)
     * @param id
     */
    @Get(":id")
    public getGroupById(@Param("id") id: string): Promise<Group> {
        if (!ObjectId.isValid(id)) {
            throw new BadRequestException("The provided ID is not a valid ObjectId.");
        }

        return this.groupService.getGroupById(id);
    }

    /**
     * Add a new Group
     *
     * Handle [POST `/api/Group`](/licentavirtualclassroom/docs/swagger/#/Group/post_Faculties)
     * @param GroupDto
     */
    @Post()
    @UseGuards(ProfessorGuard)
    @UsePipes(new ValidationPipe({ whitelist: true }))
    public postGroup(
        @Body() GroupDto: GroupDTO,
    ): Promise<IPostResponse> {
        return this.groupService.addGroup(GroupDto)
            .then((GroupId: ObjectId) => {
                return { _id: GroupId.toHexString() }
            });
    }

    /**
     * Update an existing Group by ID
     *
     * Handle [PUT `/api/Group/:id`](/licentavirtualclassroom/docs/swagger/#/Group/put_Group__id_)
     * @param id
     * @param GroupDTO
     */
    @Put(":id")
    @UseGuards(ProfessorGuard)
    @UsePipes(new ValidationPipe({ skipMissingProperties: true, whitelist: true }))
    public putGroup(
        @Param("id") id: string,
        @Body() GroupDTO: GroupDTO
    ): Promise<Group> {
        if (!ObjectId.isValid(id)) {
            throw new BadRequestException("The provided ID is not a valid ObjectId.");
        }

        return this.groupService.updateGroup(id, GroupDTO);
    }


    /**
     * Delete a Group by ID
     *
     * Handle [DELETE `/api/Group/:id`](/licentavirtualclassroom/docs/swagger/#/Group/delete_Group__id_)
     * @param id
     */
    @Delete(":id")
    @UseGuards(ProfessorGuard)
    @HttpCode(204)
    public deleteGroup(@Param("id") id: string): Promise<void> {
        if (!ObjectId.isValid(id)) {
            throw new BadRequestException("The provided ID is not a valid ObjectId.");
        }

        return this.groupService.deleteGroup(id);
    }
}

