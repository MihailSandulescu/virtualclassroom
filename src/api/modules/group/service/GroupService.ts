/**
 * @packageDocumentation
 * @module API/Group
 */

import {Inject, Injectable} from "@nestjs/common";
import {Group} from "../models/Group";
import {InstanceType} from "@typegoose/typegoose";
import {Model} from "../../mongoose/model";
import {GroupDTO} from "../models/GroupDTO";
import {NoResultException} from "../../../exceptions/models/NoResultException";
import {ObjectId} from "mongodb";

/**
 * Service containing business logic for handling Group operations
 */
@Injectable()
export class GroupService {
    constructor(
        @Inject("GroupModel") private readonly groupModel: Model<Group>
    ) {}

    /**
     * Retrieve a Group by the Group ID
     * @param id - String representation of an ObjectId
     * @returns Promise<ObjectId>
     */
    public async getGroupById(id: string): Promise<Group> {
        const Group: InstanceType<Group> | null = await this.groupModel.findById(id);

        if (!Group) {
            throw new NoResultException(`Group ${id} does not exist.`);
        }

        return Group;
    };

    /**
     * Retrieve a Group by the Group name
     * @param name - group's name
     * @returns Promise<ObjectId>
     */
    public async getGroupByName(name: string): Promise<Group> {
        const group: Group = await this.groupModel.findOne({
            name
        });

        if (!group) {
            throw new NoResultException(`Requested group does not exist.`);
        }

        return group;
    };

    /**
     * Retrieve a Group a given User is in
     * @param id - String representation of an ObjectId
     * @param populateStudents - if true, will populate the students list with instances of User
     * @returns Promise<ObjectId>
     */
    public async getUserGroup(id: string, populateStudents: boolean = false): Promise<Group> {
        let group: InstanceType<Group> | null = null;

        if (populateStudents) {
            group = await this.groupModel.findOne({
                students: id
            }).populate({ path: "students", model: "User" });
        } else {
            group = await this.groupModel.findOne({
                students: id
            });
        }

        if (!group) {
            throw new NoResultException(`User ${id} is not part of any group.`);
        }

        return group;
    };

    /**
     * Retrieves all Groups
     * @returns Promise<Array<[[Groups]]>>
     */
    public async getGroups(): Promise<Group[]> {
        return this.groupModel.find({});
    };

    /**
     * Add a new Group in the database
     * @param payload and IGroup object representing the Group that will be loaded
     * @returns Promise<ObjectId> the _id field of the inserted Group
     */
    public async addGroup(payload: GroupDTO): Promise<ObjectId> {
        const newGroup: Group = await new this.groupModel(payload).save();

        return newGroup._id;
    }

    /**
     * Update a Group
     * @param id - the _id of the Group to update
     * @param payload - the data transfer object received from the front-end
     * @return result - the modified Group
     */
    public async updateGroup(id: string, payload: GroupDTO): Promise<Group> {
        const result: InstanceType<Group> | null = await this.groupModel
            .findByIdAndUpdate(id, payload, { new: true });

        if (result === null) {
            throw new NoResultException(`Group ID ${id} does not exist.`);
        }

        return result;
    }

    /**
     * Delete a Group from the database based on its ID
     * @param id - Group's ID
     * @return Promise<void>
     */
    public async deleteGroup(id: string): Promise<void> {
        await this.groupModel.findByIdAndRemove(id);
    }
}
