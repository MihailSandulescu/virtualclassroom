/**
 * NestJS Module: **Group**
 *
 * Controller: [[GroupController]]
 *
 * Exports: [[GroupService]]
 *
 * Provides: [[GroupService]], [[GroupModel]]
 *
 * @packageDocumentation
 * @module API/Group
 * @preferred
 */

import {Module} from '@nestjs/common';
import {GroupController} from "./controller/GroupController";
import {GroupService} from "./service/GroupService";
import {GroupModel} from "./models/GroupModel";

/**
 * NestJS Module: **Group**
 *
 * Controller: [[GroupController]]
 *
 * Exports: [[GroupService]]
 *
 * Provides: [[GroupService]], [[GroupModel]]
 *
 */
@Module({
    controllers: [GroupController],
    providers: [
        GroupService,
        { provide: "GroupModel", useValue: GroupModel },
    ],
    exports: [
        GroupService,
        { provide: "GroupModel", useValue: GroupModel },
    ],
})
export class GroupModule {}
