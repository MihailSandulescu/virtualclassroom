/**
 * @packageDocumentation
 * @module API/Group
 */

import {IGroup} from "./IGroup";
import {IsString} from "class-validator";
import {ObjectId} from "bson";
import {User} from "../../user/models/User";
import {Assignment} from "../../assignments/models/Assignment";

export class GroupDTO implements IGroup {

    @IsString()
    public name: string;

    @IsString({ each: true })
    public students: (string | ObjectId | User)[];

    @IsString( { each: true })
    public assignments: (string | ObjectId | Assignment)[];

}