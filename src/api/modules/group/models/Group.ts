/**
 * @packageDocumentation
 * @module API/Group
 */
import {ObjectId} from "bson";
import {prop, Typegoose} from "@typegoose/typegoose";
import {IGroup} from "./IGroup";
import {User} from "../../user/models/User";
import {Assignment} from "../../assignments/models/Assignment";

export class Group extends Typegoose implements IGroup {
    public _id: ObjectId;

    @prop({ required: true })
    public name: string;

    @prop({ required: true, index: true })
    public students: (string | ObjectId | User)[];

    @prop({ required: true })
    public assignments: (string | ObjectId | Assignment)[];

    public createdAt: Date;

    public updatedAt: Date;
}
