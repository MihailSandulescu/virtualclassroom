/**
 * @packageDocumentation
 * @module API/Group
 */

import MongooseService from "../../mongoose/service/MongooseService";
import {Model} from "../../mongoose/model";
import {Group} from "./Group";

export const GroupModel: Model<Group> = new Group().getModelForClass(Group, {
    existingConnection: MongooseService.getInstance().getClient(),
    schemaOptions: { collection: 'Groups', versionKey: false, timestamps: true}
});
