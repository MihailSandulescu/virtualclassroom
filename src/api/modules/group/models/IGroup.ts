/**
 * @packageDocumentation
 * @module API/Group
 */

import {ObjectId} from "bson";
import {User} from "../../user/models/User";
import {Assignment} from "../../assignments/models/Assignment";

export interface IGroup {
    name: string;
    students: (string | ObjectId | User)[];
    assignments: (string | ObjectId | Assignment)[];
}