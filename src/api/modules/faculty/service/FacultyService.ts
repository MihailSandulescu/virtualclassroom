/**
 * @packageDocumentation
 * @module API/Faculty
 */

import {Inject, Injectable} from "@nestjs/common";
import {Faculty} from "../models/Faculty";
import {InstanceType} from "@typegoose/typegoose";
import {Model} from "../../mongoose/model";
import {FacultyDTO} from "../models/FacultyDTO";
import {NoResultException} from "../../../exceptions/models/NoResultException";
import {ObjectId} from "mongodb";
import {GroupService} from "../../group/service/GroupService";
import {Group} from "../../group/models/Group";

/**
 * Service containing business logic for handling Faculty operations
 */
@Injectable()
export class FacultyService {
    constructor(
        @Inject("FacultyModel") private readonly facultyModel: Model<Faculty>,
        private readonly groupService: GroupService
    ) {}

    /**
     * Retrieve a Faculty by the Faculty ID
     * @param id - String representation of an ObjectId
     * @returns Promise<ObjectId>
     */
    public async getFacultyById(id: string): Promise<Faculty> {
        const faculty: InstanceType<Faculty> | null = await this.facultyModel.findById(id)
            .populate({ path: "subjects", model: "Subject" });

        if (!faculty) {
            throw new NoResultException(`Faculty ${id} does not exist.`);
        }

        return faculty;
    };

    /**
     * Retrieve a Faculty a given group is part of
     * @param id - String representation of an ObjectId
     * @returns Promise<ObjectId>
     */
    public async getGroupFaculty(id: string): Promise<Faculty> {
        const faculty: InstanceType<Faculty> | null = await this.facultyModel.findOne({
            groups: id
        }).populate({ path: "subjects", model: "Subject" });

        if (!faculty) {
            throw new NoResultException(`Group ${id} is not part of any faculty.`);
        }

        return faculty;
    };

    /**
     * Retrieve a Faculty a given user is part of
     * @param id - String representation of an ObjectId
     * @returns Promise<ObjectId>
     */
    public async getUserFaculty(id: string): Promise<Faculty> {
        const userGroup: Group = await this.groupService.getUserGroup(id);
        const faculty: InstanceType<Faculty> | null = await this.facultyModel.findOne({
            group: userGroup._id
        }).populate({ path: "subjects", model: "Subject" });

        if (!faculty) {
            throw new NoResultException(`User ${id} is not part of any faculty.`);
        }

        return faculty;
    };

    /**
     * Retrieves all Faculties
     * @returns Promise<Array<[[Faculties]]>>
     */
    public async getFaculties(): Promise<Faculty[]> {
        return this.facultyModel.find({});
    };

    /**
     * Add a new Faculty in the database
     * @param payload and IFaculty object representing the Faculty that will be loaded
     * @returns Promise<ObjectId> the _id field of the inserted Faculty
     */
    public async addFaculty(payload: FacultyDTO): Promise<ObjectId> {
        const newFaculty: Faculty = await new this.facultyModel(payload).save();

        return newFaculty._id;
    }

    /**
     * Update a Faculty
     * @param id - the _id of the Faculty to update
     * @param payload - the data transfer object received from the front-end
     * @return result - the modified Faculty
     */
    public async updateFaculty(id: string, payload: FacultyDTO): Promise<Faculty> {
        const result: InstanceType<Faculty> | null = await this.facultyModel
            .findByIdAndUpdate(id, payload, { new: true });

        if (result === null) {
            throw new NoResultException(`Faculty ID ${id} does not exist.`);
        }

        return result;
    }

    /**
     * Delete a Faculty from the database based on an ID
     * @param id - Faculty's ID
     * @return Promise<void>
     */
    public async deleteFaculty(id: string): Promise<void> {
        await this.facultyModel.findByIdAndRemove(id);
    }
}
