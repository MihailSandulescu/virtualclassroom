/**
 * @packageDocumentation
 * @module API/Faculty
 */

import {IFaculty} from "./IFaculty";
import {IsMongoId, IsString} from "class-validator";

export class FacultyDTO implements IFaculty {

    @IsString()
    public name: string;

    @IsString()
    public address: string;

    @IsMongoId({ each: true })
    public subjects: string[];

    @IsMongoId({ each: true })
    public groups: string[];

    @IsMongoId( { each: true })
    public professors: string[];

}