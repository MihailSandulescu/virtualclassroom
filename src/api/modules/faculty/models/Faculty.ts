/**
 * @packageDocumentation
 * @module API/Faculty
 */
import {ObjectId} from "bson";
import {prop, Typegoose} from "@typegoose/typegoose";
import {IFaculty} from "./IFaculty";
import {User} from "../../user/models/User";
import {Group} from "../../group/models/Group";
import {Subject} from "../../subject/models/Subject";

export class Faculty extends Typegoose implements IFaculty {
    public _id: ObjectId;

    @prop({ required: true })
    public name: string;

    @prop({ required: true })
    public address: string;

    @prop({ required: true, index: true })
    public subjects: (string | ObjectId | Subject)[];

    @prop({ required: true, index: true })
    public groups: (string | ObjectId | Group)[];

    @prop({ required: true })
    public professors: (string | ObjectId | User)[];

    public createdAt: Date;

    public updatedAt: Date;
}
