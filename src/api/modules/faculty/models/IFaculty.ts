/**
 * @packageDocumentation
 * @module API/Faculty
 */

import {ObjectId} from "bson";
import {User} from "../../user/models/User";
import {Group} from "../../group/models/Group";
import {Subject} from "../../subject/models/Subject";

export interface IFaculty {
    name: string;
    address: string;
    subjects: (string | ObjectId | Subject)[];
    groups: (string | ObjectId | Group)[];
    professors: (string | ObjectId | User)[];
}