/**
 * @packageDocumentation
 * @module API/Faculty
 */

import MongooseService from "../../mongoose/service/MongooseService";
import {Model} from "../../mongoose/model";
import {Faculty} from "./Faculty";

export const FacultyModel: Model<Faculty> = new Faculty().getModelForClass(Faculty, {
    existingConnection: MongooseService.getInstance().getClient(),
    schemaOptions: { collection: 'Faculties', versionKey: false, timestamps: true}
});
