/**
 * NestJS Module: **Faculty**
 *
 * Controller: [[FacultyController]]
 *
 * Exports: [[FacultyService]]
 *
 * Provides: [[FacultyService]], [[FacultyModel]]
 *
 * Imports: [[GroupModule]]
 * @packageDocumentation
 * @module API/Faculty
 * @preferred
 */

import {Module} from '@nestjs/common';
import {FacultyController} from "./controller/FacultyController";
import {FacultyService} from "./service/FacultyService";
import {FacultyModel} from "./models/FacultyModel";
import {GroupModule} from "../group/GroupModule";

/**
 * NestJS Module: **Faculty**
 *
 * Controller: [[FacultyController]]
 *
 * Exports: [[FacultyService]]
 *
 * Provides: [[FacultyService]], [[FacultyModel]]
 *
 * Imports: [[GroupModule]]
 */
@Module({
    controllers: [FacultyController],
    providers: [
        FacultyService,
        { provide: "FacultyModel", useValue: FacultyModel },
    ],
    exports: [
        FacultyService,
        { provide: "FacultyModel", useValue: FacultyModel },
    ],
    imports: [GroupModule]
})
export class FacultyModule {}
