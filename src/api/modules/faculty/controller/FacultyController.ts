/**
 * @packageDocumentation
 * @module API/Faculty
 */

import {
    BadRequestException,
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    Param,
    Post,
    Put,
    Query,
    UseGuards,
    UsePipes,
    ValidationPipe
} from "@nestjs/common";
import {ObjectId} from "bson";
import {Faculty} from "../models/Faculty";
import {FacultyDTO} from "../models/FacultyDTO";
import {AuthenticatedGuard} from "../../authentication/models/guards/AuthenticatedGuard";
import {IPostResponse} from "../../../../utils";
import {AdminGuard} from "../../authentication/models/guards/AdminGuard";
import {FacultyService} from "../service/FacultyService";
import {Subject} from "../../subject/models/Subject";
import moment from "moment";
import {Utils} from "../../../../utils/Utils";

export interface IEvent {
    title: string;
    start: string;
    end: string;
    allDay: boolean;
    backgroundColor: string;
}

/**
 * Controller for handling all [`/api/Faculty`](/licentavirtualclassroom/docs/swagger/#/Faculty) routes
 */
@Controller("api/faculty")
@UseGuards(AuthenticatedGuard)
export class FacultyController {

    constructor(private readonly facultyService: FacultyService) {}

    /**
     * Retrieve a Faculty by ID
     *
     * Handle [GET `/api/Faculty`](/licentavirtualclassroom/docs/swagger/#/Faculty/get_Faculty)
     */
    @Get("")
    public getFaculties(): Promise<Faculty[]> {
        return this.facultyService.getFaculties();
    }

    /**
     * Retrieve a Faculty by ID
     *
     * Handle [GET `/api/Faculty/:id`](/licentavirtualclassroom/docs/swagger/#/Faculty/get_Faculty__id_)
     * @param id
     */
    @Get(":id")
    public getFacultyById(@Param("id") id: string): Promise<Faculty> {
        if (!ObjectId.isValid(id)) {
            throw new BadRequestException("The provided ID is not a valid ObjectId.");
        }

        return this.facultyService.getFacultyById(id);
    }

    /**
     * Retrieve the Faculty's timetable by ID, starDate and endDate
     *
     * Handle [GET `/api/Faculty/timetable/:id`](/licentavirtualclassroom/docs/swagger/#/Faculty/get_Faculty_timetable__id_)
     * @param id - the Faculty's ID
     * @param startDate - start date for the values
     * @param endDate - end date for the values
     */
    @Get("timetable/:id")
    @UsePipes(new ValidationPipe({ transform: true }))
    public async getTimetable(
        @Param("id") id: string,
        @Query("start") startDate: Date,
        @Query("end") endDate: Date,
    ): Promise<IEvent[]> {
        if (!ObjectId.isValid(id)) {
            throw new BadRequestException("The provided ID is not a valid ObjectId.");
        }

        const faculty: Faculty = await this.facultyService.getFacultyById(id);
        const subjects: Subject[] = faculty.subjects as Subject[];

        return subjects.map((s: Subject) => {
            const defaultDuration: number = 2;
            const timetable: Date[] = s.getTimetable(startDate, endDate);

            return timetable.map(d => ({
                id: s._id,
                title: s.name,
                start: moment(d).format(),
                end: moment(d).add(defaultDuration, "h").format(),
                allDay: false,
                backgroundColor: Utils.getStringColor(s.name)
            }));
        }).flat();
    }

    /**
     * Add a new Faculty
     *
     * Handle [POST `/api/Faculty`](/licentavirtualclassroom/docs/swagger/#/Faculty/post_Faculties)
     * @param FacultyDto
     */
    @Post()
    @UseGuards(AdminGuard)
    @UsePipes(new ValidationPipe({ whitelist: true }))
    public postFaculty(
        @Body() FacultyDto: FacultyDTO,
    ): Promise<IPostResponse> {
        return this.facultyService.addFaculty(FacultyDto)
            .then((FacultyId: ObjectId) => {
                return { _id: FacultyId.toHexString() }
            });
    }

    /**
     * Update an existing Faculty by ID
     *
     * Handle [PUT `/api/Faculty/:id`](/licentavirtualclassroom/docs/swagger/#/Faculty/put_Faculties__id_)
     * @param id
     * @param FacultyDTO
     */
    @Put(":id")
    @UseGuards(AdminGuard)
    @UsePipes(new ValidationPipe({ skipMissingProperties: true, whitelist: true }))
    public putFaculty(
        @Param("id") id: string,
        @Body() FacultyDTO: FacultyDTO
    ): Promise<Faculty> {
        if (!ObjectId.isValid(id)) {
            throw new BadRequestException("The provided ID is not a valid ObjectId.");
        }

        return this.facultyService.updateFaculty(id, FacultyDTO);
    }


    /**
     * Delete a Faculty by ID
     *
     * Handle [DELETE `/api/Faculty/:id`](/licentavirtualclassroom/docs/swagger/#/Faculty/delete_Faculty__id_)
     * @param id
     */
    @Delete(":id")
    @UseGuards(AdminGuard)
    @HttpCode(204)
    public deleteFaculty(@Param("id") id: string): Promise<void> {
        if (!ObjectId.isValid(id)) {
            throw new BadRequestException("The provided ID is not a valid ObjectId.");
        }

        return this.facultyService.deleteFaculty(id);
    }
}

