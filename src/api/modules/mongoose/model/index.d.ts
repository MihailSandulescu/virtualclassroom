import mongoose from "mongoose";

type Model<T> = mongoose.Model<InstanceType<T>, {}> & Typegoose & T;
