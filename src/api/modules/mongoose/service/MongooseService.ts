/**
 * @packageDocumentation
 * @module API/Mongoose
 */

import mongoose from 'mongoose';
import {Configuration, IMongoConfig} from "../../../../utils/Configuration";

/**
 * Singleton service for providing a unique, application-wide connection to MongoDB via Mongoose
 */
class MongooseService {
    private static instance: MongooseService;
    public mongooseClient: mongoose.Connection;

    private constructor() {
        const mongoConfig: IMongoConfig = Configuration.getInstance().getMongoConfig();
        this.mongooseClient = mongoose.createConnection(mongoConfig.connectionUri, {
            useUnifiedTopology: true,
            useNewUrlParser: true,
            useCreateIndex: true,
            useFindAndModify: false
        });
    }
    /**
     * Retrieves the unique instance of MongooseService
     * @returns MongooseService
     */
    public static getInstance(): MongooseService {
        if (!this.instance) {
            this.instance = new MongooseService();
        }

        return MongooseService.instance;
    }
    /**
     * Retrieves the Mongoose Client instance
     * @returns mongoose.Connection
     */
    public getClient(): mongoose.Connection {
        return this.mongooseClient;
    }
}

export default MongooseService;
