/**
 * @packageDocumentation
 * @module API/AssignmentSolution
 */
import {
    BadRequestException,
    Body,
    Controller,
    Delete,
    Get,
    Header,
    Param,
    Post,
    Put,
    Res,
    UploadedFile,
    UseGuards,
    UseInterceptors,
    UsePipes,
    ValidationPipe
} from "@nestjs/common";
import {AssignmentSolutionService} from "../service/AssignmentSolutionService";
import {StudentGuard} from "../../authentication/models/guards/StudentGuard";
import {AssignmentSolution} from "../models/AssignmentSolution";
import {AuthenticatedGuard} from "../../authentication/models/guards/AuthenticatedGuard";
import path from "path";
import {AssignmentSolutionDTO} from "../models/dto/AssignmentSolutionDTO";
import {ProfessorGuard} from "../../authentication/models/guards/ProfessorGuard";
import {Response} from "express";
import {AssignmentFileInterceptor} from "../../assignments/interceptor/AssignmentFileInterceptor";
import {LoggedUser} from "../../../decorators/LoggedUser";
import {User} from "../../user/models/User";
import {ObjectId} from "bson";
import {Assignment} from "../../assignments/models/Assignment";
import {AssignmentsService} from "../../assignments/service/AssignmentsService";

@Controller("api/assignment-solutions")
@UseGuards(AuthenticatedGuard, StudentGuard)
export class AssignmentSolutionController {

    constructor(
        private readonly assignmentSolutionService: AssignmentSolutionService,
        private readonly assignmentService: AssignmentsService
    ) {}

    /**
     * List all assignment solutions in the database
     * [GET `/api/assignment-solutions`](/licentavirtualclassroom/docs/swagger/#/Assignment%20Solutions/get_assignment_solutions)
     */
    @Get()
    public async getAssignmentSolutions(): Promise<AssignmentSolution[]> {
        return this.assignmentSolutionService.getAssignmentSolutions();
    }

    /**
     * List the current user's assignment solutions
     * [GET `/api/assignment-solutions/my`](/licentavirtualclassroom/docs/swagger/#/Assignment%20Solutions/get_assignment_solutions_my)
     */
    @Get("my")
    public async getMyAssignmentSolutions(@LoggedUser() user: User): Promise<AssignmentSolution[]> {
        return this.assignmentSolutionService.getAssignmentSolutionsForUser(user._id.toHexString());
    }

    /**
     * List all assignment solutions for a given user id
     * [GET `/api/assignment-solutions/user/:id`](/licentavirtualclassroom/docs/swagger/#/Assignment%20Solutions/get_assignment_solutions_user__id_)
     * @param userId - the user id
     */
    @Get("user/:id")
    public async getAssignmentsForUser(@Param("id") userId: string): Promise<AssignmentSolution[]> {
        return this.assignmentSolutionService.getAssignmentSolutionsForUser(userId);
    }

    /**
     * Retrieve the details of an assignment solution
     * [GET `/api/assignment-solutions/:id`](/licentavirtualclassroom/docs/swagger/#/Assignment%20Solutions/get_assignment_solutions__id_)
     * @param id
     */
    @Get(":id")
    public async getAssignmentSolution(@Param("id") id: string): Promise<AssignmentSolution> {
        return this.assignmentSolutionService.getAssignmentSolution(id);
    }

    /**
     * Download an assignment upload
     * [GET `/api/assignment-solutions/:id/download`](/licentavirtualclassroom/docs/swagger/#/Assignment%20Solutions/get_assignment_solutions__id__download)
     * @param id - the assignment upload
     * @param res - Express.Response object
     */
    @Get(":id/download")
    @Header("Content-Type", "application/octet-stream")
    public async getAssignmentSolutionUpload(
        @Param("id") id: string,
        @Res() res: Response
    ): Promise<void> {
        const assignmentSolution: AssignmentSolution = await this.assignmentSolutionService.getAssignmentSolution(id);

        return res.download(path.resolve(__dirname, "../../../../", assignmentSolution.file));
    }

    /**
     * Adds a new assignment solution.
     * [POST `/api/assignment-solutions`](/licentavirtualclassroom/docs/swagger/#/Assignment%20Solutions/post_assignment_solutions)
     * @param user - the logged in user
     * @param assignmentId - the provided assignment id
     * @param file - attachments
     */
    @Post(":assignmentId")
    @UseInterceptors(AssignmentFileInterceptor("file"))
    @UsePipes(new ValidationPipe({ whitelist: true }))
    public async postAssignmentSolution(
        @LoggedUser() user: User,
        @Param('assignmentId') assignmentId: string,
        @UploadedFile() file: Express.Multer.File,
    ): Promise<AssignmentSolution> {
        if (!ObjectId.isValid(assignmentId)) {
            throw new BadRequestException("The provided ID is not a valid ObjectId.");
        }

        const assignment: Assignment = await this.assignmentService.getAssignment(assignmentId);

        let segments: string[] = file.destination.split("/");
        segments = segments.slice(Math.max(segments.length - 3, 0));

        const assignmentSolutionDto: AssignmentSolutionDTO = new AssignmentSolutionDTO();
        Object.assign(assignmentSolutionDto, {
            file: path.join(...segments, file.filename),
            assignment: assignment._id.toHexString(),
            student: user._id.toHexString()
        });

        return this.assignmentSolutionService.addAssignmentSolution(assignmentSolutionDto);
    }

    /**
     * Modify an existing assignment solution.
     * [PUT `/api/assignment-solutions/:id`](/licentavirtualclassroom/docs/swagger/#/Assignment%20Solutions/put_assignment_solutions__id_)
     * @param id - the assignment id
     * @param assignmentSolutionDto - the new assignment object
     * @param file - attachments
     */
    @Put(":id")
    @UseGuards(ProfessorGuard)
    @UseInterceptors(AssignmentFileInterceptor("file"))
    @UsePipes(new ValidationPipe({ skipMissingProperties: true, whitelist: true }))
    public async putAssignmentSolution(
        @Param("id") id: string,
        @Body() assignmentSolutionDto: AssignmentSolutionDTO,
        @UploadedFile() file: Express.Multer.File
    ): Promise<AssignmentSolution> {
        let segments: string[] = file.destination.split("/");
        segments = segments.slice(Math.max(segments.length - 3, 0));
        assignmentSolutionDto.file = path.join(...segments, file.filename);

        return this.assignmentSolutionService.updateAssignmentSolution(id, assignmentSolutionDto);
    }

    /**
     * Delete an existing assignment. Only Professors can do that.
     * [DELETE `/api/assignment-solutions/:id`](/licentavirtualclassroom/docs/swagger/#/Assignment%20Solutions/delete_assignment_solutions__id_)
     * @param id - the assignment id
     */
    @Delete(":id")
    @UseGuards(ProfessorGuard)
    public async deleteAssignmentSolution(@Param("id") id: string): Promise<AssignmentSolution> {
        return this.assignmentSolutionService.deleteAssignmentSolution(id);
    }

}