/**
 * @packageDocumentation
 * @module API/AssignmentSolution
 */
import {IAssignmentSolution} from "../IAssignmentSolution";
import {IsMongoId} from "class-validator";

export class AssignmentSolutionDTO implements IAssignmentSolution {
    public file: string;

    @IsMongoId()
    public assignment: string;

    @IsMongoId()
    public student: string;
}