/**implements AssignmentDTO
 * @packageDocumentation
 * @module API/AssignmentSolution
 */
import {ObjectId} from "bson";
import {User} from "../../user/models/User";
import {Assignment} from "../../assignments/models/Assignment";

export interface IAssignmentSolution {
    file: string;
    assignment: ObjectId | Assignment | string;
    student: ObjectId | User | string;
}