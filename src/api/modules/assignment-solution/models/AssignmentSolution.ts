/**
 * @packageDocumentation
 * @module API/AssignmentSolution
 */
import {InstanceType, prop, Typegoose} from "@typegoose/typegoose";
import {ObjectId} from "bson";
import {IAssignmentSolution} from "./IAssignmentSolution";
import mongoose from "mongoose";
import MongooseService from "../../mongoose/service/MongooseService";
import {User} from "../../user/models/User";
import {Assignment} from "../../assignments/models/Assignment";

export class AssignmentSolution extends Typegoose implements IAssignmentSolution {
    public _id: ObjectId;

    @prop({ required: true })
    public file: string;

    @prop({ required: true, index: true })
    public assignment: ObjectId | Assignment | string;

    @prop({ default: [], index: true })
    public student: ObjectId | User | string;

    public createdAt: Date;

    public updatedAt: Date;
}

export const AssignmentSolutionModel: mongoose.Model<InstanceType<AssignmentSolution>> = new AssignmentSolution().getModelForClass(AssignmentSolution, {
    existingConnection: MongooseService.getInstance().getClient(),
    schemaOptions: { collection: 'AssignmentSolution', versionKey: false, timestamps: true}
});