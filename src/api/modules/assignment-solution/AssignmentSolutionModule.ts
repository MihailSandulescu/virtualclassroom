/**
 * NestJS Module: **AssignmentSolution**
 *
 * Controller: [[AssignmentSolutionController]]
 *
 * Exports: [[AssignmentSolutionService]]
 *
 * Imports: [[UserService]], [[AssignmentsModule]]
 *
 * Provides: [[AssignmentSolutionService]], [[AssignmentSolutionModel]]
 * @packageDocumentation
 * @module API/AssignmentSolution
 * @preferred
 */

import {Module} from "@nestjs/common";
import {AssignmentSolutionModel} from "./models/AssignmentSolution";
import {AssignmentSolutionController} from "./controller/AssignmentSolutionController";
import {AssignmentSolutionService} from "./service/AssignmentSolutionService";
import {UserModule} from "../user/UserModule";
import {AssignmentsModule} from "../assignments/AssignmentsModule";

/**
 * NestJS Module: **AssignmentSolution**
 *
 * Controller: [[AssignmentSolutionController]]
 *
 * Imports: [[UserService]], [[AssignmentsModule]]
 *
 * Exports: [[AssignmentSolutionService]]
 *
 * Provides: [[AssignmentSolutionService]], [[AssignmentSolutionModel]]
 */
@Module({
    controllers: [AssignmentSolutionController],
    imports: [UserModule, AssignmentsModule],
    exports: [
        AssignmentSolutionService
    ],
    providers: [
        AssignmentSolutionService,
        { provide: "AssignmentSolutionModel", useValue: AssignmentSolutionModel },
    ]
})
export class AssignmentSolutionModule {}