/**
 * @packageDocumentation
 * @module API/AssignmentSolution
 */
import {Inject, Injectable, NotFoundException} from "@nestjs/common";
import mongoose from "mongoose";
import {InstanceType} from "@typegoose/typegoose";
import {AssignmentSolution} from "../models/AssignmentSolution";
import {NoResultException} from "../../../exceptions/models/NoResultException";
import {AssignmentSolutionDTO} from "../models/dto/AssignmentSolutionDTO";
import fs from "fs";
import path from "path";

@Injectable()
export class AssignmentSolutionService {

    constructor(
        @Inject("AssignmentSolutionModel") private readonly assignmentSolutionModel: mongoose.Model<InstanceType<AssignmentSolution>>
    ) {}

    /**
     * Retrieve all existing assignments solutions
     */
    public async getAssignmentSolutions(): Promise<AssignmentSolution[]> {
        const assignmentSolutions: AssignmentSolution[] = await this.assignmentSolutionModel.find({});

        return assignmentSolutions;
    }

    /**
     * Retrieve an assignment solution by id
     * @param id - the assignment id
     */
    public async getAssignmentSolution(id: string): Promise<AssignmentSolution> {
        const assignmentSolution: AssignmentSolution | null = await this.assignmentSolutionModel.findById(id);
        if (!assignmentSolution) throw new NoResultException(`Assignment solution id ${id} does not exist`);
        return assignmentSolution;
    }

    /**
     * Retrieve all assignment solutions belonging to a user by user id
     * @param assignmentId - the assignment's id
     * @param userId - the user id
     */
    public async getAssignmentSolutionsForAssignmentAndUser(assignmentId: string, userId: string): Promise<AssignmentSolution | null> {
        const assignmentSolution: AssignmentSolution | null = await this.assignmentSolutionModel.findOne({
            student: userId,
            assignment: assignmentId
        });

        return assignmentSolution;
    }

    /**
     * Retrieve all assignment solutions belonging to a user by user id
     * @param userId - the user id
     */
    public async getAssignmentSolutionsForUser(userId: string): Promise<AssignmentSolution[]> {
        const assignmentSolutions: AssignmentSolution[] = await this.assignmentSolutionModel.find({
            student: userId
        });

        return assignmentSolutions;
    }

    /**
     * Retrieve all assignments solutions belonging to an assignment by id
     * @param id - the assignment solution id
     */
    public async getAssignmentSolutionsForAssignment(id: string): Promise<AssignmentSolution[]> {
        const assignmentSolutions: AssignmentSolution[] = await this.assignmentSolutionModel.find({
            assignment: id
        }).populate({ path: "student", model: "User" });

        return assignmentSolutions;
    }
    /**
     * Create a new assignment solution
     * @param assignmentSolution - the assignment solution object
     */
    public async addAssignmentSolution(assignmentSolution: AssignmentSolutionDTO): Promise<AssignmentSolution> {
        const model: InstanceType<AssignmentSolution> = new this.assignmentSolutionModel(assignmentSolution);
        const newAssignmentSolution: AssignmentSolution = await model.save();

        return newAssignmentSolution;
    }

    /**
     * Update an existing assignment solution
     * @param id - the assignment solution id
     * @param assignmentSolutionDto - the assignment solution object
     */
    public async updateAssignmentSolution(id: string, assignmentSolutionDto: AssignmentSolutionDTO): Promise<AssignmentSolution> {
        const assignmentSolution: InstanceType<AssignmentSolution> | null = await this.assignmentSolutionModel.findByIdAndUpdate(id, {
            $set: {
                ...assignmentSolutionDto,
            }
        }, { new: true });

        if (!assignmentSolution) throw new NoResultException(`Assignment solution id ${id} does not exist`);

        return assignmentSolution;
    }

    /**
     * Delete an existing assignment solution
     * @param assignmentSolutionId - the assignment solution id
     */
    public async deleteAssignmentSolution(assignmentSolutionId: string): Promise<AssignmentSolution> {
        const deletedAssignmentSolution: AssignmentSolution | null = await this.assignmentSolutionModel.findByIdAndRemove(assignmentSolutionId);
        if (!deletedAssignmentSolution) throw new NotFoundException(`Assignment solution id ${assignmentSolutionId} does not exist`);

        fs.unlinkSync(path.resolve(__dirname, "../../../../", deletedAssignmentSolution.file));

        return deletedAssignmentSolution;
    }
}