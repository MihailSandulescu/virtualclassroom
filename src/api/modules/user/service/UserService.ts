/**
 * @packageDocumentation
 * @module API/Users
 */


import {Inject, Injectable} from "@nestjs/common";
import {User} from "../models/User";
import {InstanceType} from "@typegoose/typegoose";
import {Model} from "../../mongoose/model";
import bcrypt from "bcrypt";
import {RoleService} from "../../role/service/RoleService";
import {AccessRole, Role} from "../../role/models/Role";
import {UserDTO} from "../models/UserDTO";
import {NoResultException} from "../../../exceptions/models/NoResultException";
import {ObjectId} from "mongodb";

/**
 * Service class for manipulating [[User]]s
 */
@Injectable()
export class UserService {
    constructor(
        private readonly roleService: RoleService,
        @Inject("UserModel") private readonly userModel: Model<User>
    ) {}

    /**
     * Retrieve a User given the user ID
     * @param id - the [[User]] ID
     * @returns Promise<ObjectId>
     */
    public async getUser(id: string): Promise<User> {
        const user: InstanceType<User> | null = await this.userModel.findById(id, { password: 0 })
            .populate({ path: "role", model: "Role" });

        if (!user) {
            throw new NoResultException(`User ${id} does not exist.`);
        }

        return user;
    };

    /**
     * Retrieve a User given the username
     * @returns Promise<ObjectId>
     * @param identifier - email or phone number
     * @param password - boolean that specifies whether you want the password
     */
    public async getUserByEmailOrPhone(identifier: string, password: boolean = true): Promise<User> {
        const user: User = await this.userModel.findOne({
            $or: [
                { email: identifier },
                { phone: identifier }
            ]
        }, { password: password ? 1 : 0 })
            .populate({ path: "role", model: "Role" });

        if (!user) {
            throw new NoResultException(`User with identifier ${identifier} does not exist.`);
        }

        return user;
    };

    /**
     * Retrieves all [[User]]s
     * @returns Promise<Array<[[User]]>>
     */
    public async getUsers(): Promise<User[]> {
        return this.userModel.find({}, { password: 0 })
            .populate({path: "role", model: "Role"});
    };

    /**
     * Add a new Article in the database
     * @param payload and IArticle object representing the Article that will be loaded
     * @returns Promise<ObjectId> the _id field of the inserted Article
     */
    public async addUser(payload: UserDTO): Promise<User> {
        if (payload.password) {
            const salt: string = bcrypt.genSaltSync(10);
            const hash: string = bcrypt.hashSync(payload.password, salt);

            Object.assign(payload, { password: hash });
        } else { delete payload.password }

        const newUser: User = await new this.userModel(payload).save();

        const role: Role = await this.roleService.addRole({
            userId: newUser._id,
            roleLevel: payload.role as AccessRole
        });

        const userDto: UserDTO = new UserDTO();
        Object.assign(userDto, { role})

        return this.userModel.findByIdAndUpdate(newUser._id, { $set: { role: role._id } }, { new: true });
    }

    /**
     * Updates an existing [[User]]
     * @param id - the [[User]]'s _id
     * @param payload - the [[User]] object
     * @returns Promise<User>
     */
    public async updateUser(id: string, payload: UserDTO): Promise<User> {
        if (payload.password) {
            const salt: string = bcrypt.genSaltSync(10);
            const hash: string = bcrypt.hashSync(payload.password, salt);

            Object.assign(payload, { password: hash });
        } else { delete payload.password }

        const user: InstanceType<User> = await this.userModel.findOne({ _id: id })
            .populate({ path: "role", model: "Role" });

        if (!user) {
            throw new NoResultException(`User id ${id} does not exist.`);
        }

        if (payload.role) {
            await this.roleService.updateRole(user._id, payload.role as AccessRole);
        }

        delete payload.role;
        Object.assign(user, payload);
        return user.save();
    }

    /**
     * Deletes an existing [[User]]
     * @param id - the [[User]]'s _id
     * @returns Promise<void>
     */
    public async deleteUser(id: string): Promise<void> {
        const user: InstanceType<User> | null = await this.userModel.findByIdAndRemove(id);
        if (!user) throw new NoResultException(`User id ${id} does not exist.`);
        const role: ObjectId = user.role as ObjectId;

        await this.roleService.deleteRole(role.toHexString());
    }
}
