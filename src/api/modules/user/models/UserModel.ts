/**
 * @packageDocumentation
 * @module API/Users
 */

import MongooseService from "../../mongoose/service/MongooseService";
import {Model} from "../../mongoose/model";
import {User} from "./User";

export const UserModel: Model<User> = new User().getModelForClass(User, {
    existingConnection: MongooseService.getInstance().getClient(),
    schemaOptions: { collection: 'Users', versionKey: false, timestamps: true}
});
