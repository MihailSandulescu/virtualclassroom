/**
 * @packageDocumentation
 * @module API/Users
 */

import {IsEmail, IsEnum, IsISO8601, IsMongoId, IsNumber, IsOptional, IsPhoneNumber, IsString} from "class-validator";
import {IProfessorUser} from "./IProfessorUser";
import {IStudentUser} from "./IStudentUser";
import {AccessRole} from "../../role/models/Role";

export class UserDTO implements IProfessorUser, IStudentUser {
    @IsString()
    public firstName: string;

    @IsString()
    public lastName: string;

    @IsString()
    public password: string;

    @IsISO8601()
    public birthDate: Date;

    @IsEmail()
    public email: string;

    @IsPhoneNumber("ZZ")
    public phone: string;

    @IsNumber()
    public startYear: number;

    @IsEnum(AccessRole)
    public role: AccessRole;

    @IsOptional()
    @IsMongoId()
    public faculty?: string;

    @IsOptional()
    @IsMongoId()
    public group?: string;
}