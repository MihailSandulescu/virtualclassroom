/**
 * @packageDocumentation
 * @module API/Users
 */
import {ObjectId} from "bson";
import {prop, Typegoose} from "@typegoose/typegoose";
import {IStudentUser} from "./IStudentUser";
import {IProfessorUser} from "./IProfessorUser";
import {Role} from "../../role/models/Role";
import {Group} from "../../group/models/Group";

export class User extends Typegoose implements IStudentUser, IProfessorUser {
    public _id: ObjectId;

    @prop({ required: true })
    public firstName: string;

    @prop({ required: true })
    public lastName: string;

    @prop({ required: true })
    public birthDate: Date;

    @prop({ required: true })
    public email: string;

    @prop({ required: true })
    public password: string;

    @prop({ required: true })
    public phone: string;

    @prop({ required: true })
    public startYear: number;

    @prop()
    public group: (string | ObjectId | Group);

    @prop()
    public institution?: any;

    @prop({ required: true })
    public role: ObjectId | Role;

    public createdAt: Date;

    public updatedAt: Date;
}
