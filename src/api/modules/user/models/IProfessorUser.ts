/**
 * @packageDocumentation
 * @module API/Users
 */

import {ObjectId} from "bson";
import {AccessRole, Role} from "../../role/models/Role";

export interface IProfessorUser {
    firstName: string;
    lastName: string;
    birthDate: Date;
    email: string;
    phone: string;
    startYear: number;
    role: AccessRole | ObjectId | Role;
    institution?: any;
}