/**
 * @packageDocumentation
 * @module API/Users
 */

import {
    BadRequestException,
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    Param,
    Post,
    Put,
    Request,
    UseGuards,
    UsePipes,
    ValidationPipe
} from "@nestjs/common";
import {ObjectId} from "bson";
import {UserService} from "../service/UserService";
import {User} from "../models/User";
import {AuthenticatedGuard} from "../../authentication/models/guards/AuthenticatedGuard";
import {UserDTO} from "../models/UserDTO";
import {ProfessorGuard} from "../../authentication/models/guards/ProfessorGuard";
import {AdminGuard} from "../../authentication/models/guards/AdminGuard";
import {IPostResponse} from "../../../../utils";


/**
 * Controller for handling all [`/api/users`](/licentavirtualclassroom/docs/swagger/#/User) routes
 */
@Controller("api/users")
@UseGuards(AuthenticatedGuard)
export class UserController {

    constructor(private readonly userService: UserService) {}

    /**
     * Retrieve all Users.
     *
     * Handle [GET `/api/users`](/licentavirtualclassroom/docs/swagger/#/User/get_users)
     */
    @Get()
    @UseGuards(ProfessorGuard)
    public getUsers(): Promise<User[]> {
        return this.userService.getUsers();
    }

    /**
     * Retrieve the caller's User details
     *
     * Handle [GET `/api/users/me`](/licentavirtualclassroom/docs/swagger/#/User/get_users_me)
     */
    @Get("me")
    public getMe(@Request() req: Express.Request): User {
        return req.user as User;
    }

    /**
     * Retrieve an User by ID
     *
     * Handle [GET `/api/users/:id`](/licentavirtualclassroom/docs/swagger/#/User/get_users__id_)
     * @param id
     */
    @Get(":id")
    @UseGuards(ProfessorGuard)
    public getUser(@Param("id") id: string): Promise<User> {
        if (!ObjectId.isValid(id)) {
            throw new BadRequestException("The provided ID is not a valid ObjectId.");
        }

        return this.userService.getUser(id);
    }

    /**
     * Add a new Article
     *
     * Handle [POST `/api/users`](/licentavirtualclassroom/docs/swagger/#/User/post_users)
     * @param userDto
     */
    @Post()
    @UseGuards(AdminGuard)
    @UsePipes(new ValidationPipe({ whitelist: true }))
    public postUser(
        @Body() userDto: UserDTO,
    ): Promise<IPostResponse> {

        return this.userService.addUser(userDto)
            .then((user: User) => {
                return { _id: user._id.toHexString() }
            });
    }

    /**
     * Update an existing User by ID
     *
     * Handle [PUT `/api/users/:id`](/licentavirtualclassroom/docs/swagger/#/User/put_users__id_)
     * @param id
     * @param userDto
     */
    @Put(":id")
    @UseGuards(AdminGuard)
    @UsePipes(new ValidationPipe({ skipMissingProperties: true, whitelist: true }))
    public putUser(
        @Param("id") id: string,
        @Body() userDto: UserDTO
    ): Promise<User> {
        if (!ObjectId.isValid(id)) {
            throw new BadRequestException("The provided ID is not a valid ObjectId.");
        }

        return this.userService.updateUser(id, userDto);
    }


    /**
     * Delete an User by ID
     *
     * Handle [DELETE `/api/users/:id`](/licentavirtualclassroom/docs/swagger/#/User/delete_users__id_)
     * @param id
     */
    @Delete(":id")
    @UseGuards(AdminGuard)
    @HttpCode(204)
    public deleteUser(@Param("id") id: string): Promise<void> {
        if (!ObjectId.isValid(id)) {
            throw new BadRequestException("The provided ID is not a valid ObjectId.");
        }

        return this.userService.deleteUser(id);
    }
}

