/**
 * NestJS Module: **User**
 *
 * Controller: [[UserController]]
 *
 * Exports: [[UserService]]
 *
 * Provides: [[UserService]], [[UserModel]]
 *
 * Imports: [[RoleModule]]
 * @packageDocumentation
 * @module API/Users
 * @preferred
 */

import { Module } from '@nestjs/common';
import {UserService} from "./service/UserService";
import {UserModel} from "./models/UserModel";
import {RoleModule} from "../role/RoleModule";
import {UserController} from "./controller/UserController";

/**
 * NestJS Module: **User**
 *
 * Controller: [[UserController]]
 *
 * Exports: [[UserService]]
 *
 * Provides: [[UserService]], [[UserModel]]
 *
 * Imports: [[RoleModule]]
 */
@Module({
    controllers: [UserController],
    providers: [
        UserService,
        { provide: "UserModel", useValue: UserModel },
    ],
    exports: [
        UserService,
        { provide: "UserModel", useValue: UserModel },
    ],
    imports: [RoleModule]
})
export class UserModule {}
