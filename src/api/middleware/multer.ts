/**
 * @packageDocumentation
 * @module API
 */

import multer from "multer";
import path from "path";
import fs from "fs";
import {BadRequestException} from "@nestjs/common";

export function multerBuilder(fileTypes: string[], destination: string): multer.Options {
    const storage: multer.StorageEngine = multer.diskStorage({
        destination: (req: Express.Request, file: Express.Multer.File, cb: any): void => {
            const destinationFolder: string = `${path.dirname(require.main!.filename)}/uploads/${destination}`;

            if (!fs.existsSync(destinationFolder)) {
                fs.mkdirSync(destinationFolder, { recursive: true });
            }

            cb(null, destinationFolder);
        },
        filename: (req: Express.Request, file: Express.Multer.File, cb: any): void => {
            cb(null, file.originalname);
        }
    });

    return {
        fileFilter: (req: Express.Request, file: Express.Multer.File, cb: any) => {
            if (fileTypes.find(t => t === file.mimetype)) {
                cb(null, true);
            } else {
                cb(
                    new BadRequestException(`Tipul de fisier ${file.mimetype} nu este permis`), false
                );
            }
        },
        storage: storage
    };
}