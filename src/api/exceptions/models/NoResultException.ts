/**
 * @packageDocumentation
 * @module API
 */

/**
 * Exception indicating that the requested resource has not been found
 */
export class NoResultException extends Error {
    constructor(message: string) {
        super(message);
    }
}