/**
 * @packageDocumentation
 * @module API
 */

import {BadRequestException} from "@nestjs/common";

/**
 * Exception indicating that the requested resource has not been found
 */
export class AuthenticationException extends BadRequestException {
    constructor(message: string) {
        super(message);
    }
}