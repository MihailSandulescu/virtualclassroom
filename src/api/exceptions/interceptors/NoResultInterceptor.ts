/**
 * @packageDocumentation
 * @module API
 */

import { NoResultException } from "../models/NoResultException";
import { CallHandler, ExecutionContext, Injectable, NestInterceptor, NotFoundException } from "@nestjs/common";
import { Observable } from "rxjs";
import { catchError } from "rxjs/operators";

/**
 * Nest interceptor that intercepts a [[NoResultException]] and turns it into a NotFoundException
 */
@Injectable()
export class NoResultInterceptor implements NestInterceptor {
    /**
     * Intercept a [[NoResultException]] and turn it into a NotFoundException
     * @param context
     * @param next
     */
    public intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        return next.handle()
        .pipe(catchError(error => {
            if (error instanceof NoResultException) {
                throw new NotFoundException(error.message);
            } else {
                throw error;
            }
        }));
    }
}