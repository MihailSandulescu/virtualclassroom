/**
 * @packageDocumentation
 * @module API
 */

import {AuthenticationException} from "../models/AuthenticationException";
import {CallHandler, ExecutionContext, Injectable, NestInterceptor, UnauthorizedException} from "@nestjs/common";
import {Observable} from "rxjs";
import {catchError} from "rxjs/operators";

/**
 * Nest interceptor that intercepts a [[AuthenticationException]] and turns it into a NotFoundException
 */
@Injectable()
export class AuthenticationInterceptor implements NestInterceptor {
    /**
     * Intercept a [[AuthenticationException]] and turn it into a UnauthorizedException
     * @param context
     * @param next
     */
    public intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        return next.handle()
        .pipe(catchError(error => {
            if (error instanceof AuthenticationException) {
                throw new UnauthorizedException(error.message);
            } else {
                throw error;
            }
        }));
    }
}