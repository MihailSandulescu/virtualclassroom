/**
 * @packageDocumentation
 * @module Utils
 */

export interface IPostResponse {
    _id: string;
}