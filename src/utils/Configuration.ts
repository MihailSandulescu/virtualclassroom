/**
 * @packageDocumentation
 * @module Utils
 */
// @ts-ignore
import yml from "node-yaml";

interface IMongoConfig {
    connectionUri: string;
}

interface IExpressConfig {
    port: number;
}

interface IAuthConfig {
    /** Token sign algorithm */
    alg: "HS256" | "HS384" | "HS512" | "RS256" | "RS384" | "RS512" | "ES256" | "ES384" | "ES512" | "PS256" | "PS384" | "PS512" | "none";

    /** Token expiry time */
    expiration: string;

    /** Token availability start time */
    notBefore: string;

    /** Token audience */
    audience: string;

    /** Token issuer */
    issuer: string;

    /** Path to the public key */
    pubKeyPath: string;

    /** Path to the private key */
    privKeyPath: string;
}

interface IEnvConfig {
    mongo: IMongoConfig;
    express: IExpressConfig;
    auth: IAuthConfig;
}

interface IConfig {
    [env: string]: IEnvConfig;
}

/**
 * Singleton that holds the configuration for this server
 */
class Configuration {

    private static instance: Configuration;
    private readonly config: IEnvConfig;

    private constructor(configPath: string) {
        const env: string | undefined = process.env.NODE_ENV;

        if (!env) {
            throw new Error("NODE_ENV not set");
        }

        this.config = this.insertEnv(yml.readSync(configPath))[env];
    }

    /**
     * Retrieves the singleton instance of Configuration
     * @returns Configuration
     */
    public static getInstance(): Configuration {
        if (!this.instance) {
            this.instance = new Configuration("../config/config.yml");
        }

        return Configuration.instance;
    }

    /**
     * Retrieves the entire configuration
     * @returns IEnvConfig
     */
    public getConfig(): IEnvConfig {
        return this.config;
    }

    /**
     * Retrieves the MongoDB configuration
     * @returns IMongoConfig
     */
    public getMongoConfig(): IMongoConfig {
        if (!this.config.mongo) {
            throw new Error("MongoDB config is not defined in the config file.");
        }

        return this.config.mongo;
    }

    /**
     * Retrieves the Express configuration
     * @returns IExpressConfig
     */
    public getExpressConfig(): IExpressConfig {
        if (!this.config.express) {
            throw new Error("Express config is not defined in the config file.");
        }

        return this.config.express;
    }

    /**
     * Retrieves the Authentication configuration
     * @returns IExpressConfig
     */
    public getAuthConfig(): IAuthConfig {
        if (!this.config.auth) {
            throw new Error("Authentication config is not defined in the config file.");
        }

        return this.config.auth;
    }

    /**
     * Inserts environment variables into the config
     * @param config - the configuration read from the file
     */
    private insertEnv(config: IConfig): IConfig {
        // Replace environment variable references
        let stringifiedConfig: string = JSON.stringify(config);
        const envRegex: RegExp = /\${(\w+\b):?(\w+\b)?}/g;
        const matches: RegExpMatchArray | null = stringifiedConfig.match(envRegex);

        if (matches) {
            matches.forEach((match: string) => {
                envRegex.lastIndex = 0;
                const captureGroups: RegExpExecArray = envRegex.exec(match) as RegExpExecArray;

                // Insert the environment variable if available. If not, insert placeholder. If no placeholder, leave it as is.
                stringifiedConfig = stringifiedConfig.replace(match, (process.env[captureGroups[1]] || captureGroups[2] || captureGroups[1]));
            });
        }

        return JSON.parse(stringifiedConfig);
    }


}

export { Configuration, IMongoConfig, IExpressConfig, IEnvConfig, IConfig, IAuthConfig };
