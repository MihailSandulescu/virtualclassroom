/**
 * @packageDocumentation
 * @module Utils
 */

export class Utils {

    /**
     * Generate a unique color for a given string
     * (!) Will have collisions as there are exactly 65535 colors and an infinte number of strings
     * @param string
     */
    public static getStringColor(input: string): string {
        /**
         * Get the hash code of a string
         * @param str - the string
         */
        const getHashCode: (str: string) => number = (str: string): number => {
            let hash: number = 0;
            for (let i: number = 0; i < str.length; i++) {
                hash = str.charCodeAt(i) + ((hash << 5) - hash);
            }
            return hash;
        }

        /**
         * Get the RGB representation of a number
         * @param i - the numer
         */
        const intToRGB: (i: number) => string = (i: number): string => {
            const c: string = (i & 0x00FFFFFF)
                .toString(16)
                .toUpperCase();

            return "00000".substring(0, 6 - c.length) + c;
        }

        return `#${intToRGB(getHashCode(input))}`;
    }
}