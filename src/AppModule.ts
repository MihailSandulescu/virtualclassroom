/**
 * NestJS Module: **AppModule**
 *
 * Imports: [[APIModule]], [[UIModule]]
 * @packageDocumentation
 * @module App
 * @preferred
 */

import {Module} from '@nestjs/common';
import {APP_INTERCEPTOR} from "@nestjs/core";
import {NoResultInterceptor} from "./api/exceptions/interceptors/NoResultInterceptor";
import {AuthenticationInterceptor} from "./api/exceptions/interceptors/AuthenticationInterceptor";
import {APIModule} from "./api/APIModule";
import {UIModule} from "./ui/UIModule";

/**
 * NestJS Module: **AppModule**
 *
 * Imports: [[APIModule]], [[UIModule]]
 */
@Module({
    imports: [
        APIModule,
        UIModule
    ],
    providers: [
        { provide: APP_INTERCEPTOR, useClass: NoResultInterceptor },
        { provide: APP_INTERCEPTOR, useClass: AuthenticationInterceptor },
    ]
})
export class AppModule {}
