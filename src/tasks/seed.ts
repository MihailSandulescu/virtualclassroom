import {UserModel} from "../api/modules/user/models/UserModel";
import bcrypt from "bcrypt";
import {User} from "../api/modules/user/models/User";
import {AccessRole, Role} from "../api/modules/role/models/Role";
import {UserDTO} from "../api/modules/user/models/UserDTO";
import {RoleModel} from "../api/modules/role/models/RoleModel";
import {GroupModel} from "../api/modules/group/models/GroupModel";
import {Group} from "../api/modules/group/models/Group";
import {Faculty} from "../api/modules/faculty/models/Faculty";
import {FacultyModel} from "../api/modules/faculty/models/FacultyModel";
import {SubjectModel} from "../api/modules/subject/models/SubjectModel";
import {Subject} from "../api/modules/subject/models/Subject";

async function makeAdmin(): Promise<User> {
    const payload: any = {
        firstName: "Admin",
        lastName: "VirtualClassroom",
        birthDate: new Date("1970-01-01"),
        email: "admin@virtualclassroom.ro",
        password: "1234",
        phone: "0770123456",
        startYear: 1970,
        role: AccessRole.AdminRole
    };

    const salt: string = bcrypt.genSaltSync(10);
    const hash: string = bcrypt.hashSync(payload.password, salt);

    Object.assign(payload, { password: hash });

    const newUser: User = await new UserModel(payload).save();

    const role: Role = await new RoleModel({
        userId: newUser._id,
        roleLevel: payload.role as AccessRole
    }).save();

    const userDto: UserDTO = new UserDTO();
    Object.assign(userDto, { role})

    return await UserModel.findByIdAndUpdate(newUser._id, { $set: { role: role._id } }, { new: true });
}

async function makeProfessors(): Promise<User[]> {
    const salt: string = bcrypt.genSaltSync(10);

    const payload1: any = {
        firstName: "Profesor1",
        lastName: "Profesor1",
        birthDate: new Date("1970-01-02"),
        email: "profesor1@virtualclassroom.ro",
        password: bcrypt.hashSync("parola1", salt),
        phone: "0770123456",
        startYear: 1982,
        role: AccessRole.ProfessorRole
    };

    const payload2: any = {
        firstName: "Profesor2",
        lastName: "Profesor2",
        birthDate: new Date("1970-01-03"),
        email: "profesor2@virtualclassroom.ro",
        password: bcrypt.hashSync("parola2", salt),
        phone: "0770000456",
        startYear: 1999,
        role: AccessRole.ProfessorRole
    };

    Object.assign(payload1);
    Object.assign(payload2);

    const newProfessor1: User = await new UserModel(payload1).save();
    const newProfessor2: User = await new UserModel(payload2).save();

    const role1: Role = await new RoleModel({
        userId: newProfessor1._id,
        roleLevel: payload1.role as AccessRole
    }).save();

    const role2: Role = await new RoleModel({
        userId: newProfessor2._id,
        roleLevel: payload2.role as AccessRole
    }).save();

    const professor1Dto: UserDTO = new UserDTO();
    Object.assign(professor1Dto, { role1 })
    const professor2Dto: UserDTO = new UserDTO();
    Object.assign(professor1Dto, { role2 })

    const p1: any = UserModel.findByIdAndUpdate(newProfessor1._id, { $set: { role: role1._id } }, { new: true });
    const p2: any = UserModel.findByIdAndUpdate(newProfessor2._id, { $set: { role: role2._id } }, { new: true });

    return Promise.all([p1, p2]);
}

async function makeStudents(): Promise<User[]> {
    const salt: string = bcrypt.genSaltSync(10);

    const payload1: any = {
        firstName: "Elev1",
        lastName: "Elev1",
        birthDate: new Date("2000-01-02"),
        email: "elev1@virtualclassroom.ro",
        password: bcrypt.hashSync("parolaelev1", salt),
        phone: "0712345678",
        startYear: 2015,
        role: AccessRole.StudentRole
    };

    const payload2: any = {
        firstName: "Elev2",
        lastName: "Elev2",
        birthDate: new Date("1999-01-03"),
        email: "elev2@virtualclassroom.ro",
        password: bcrypt.hashSync("parolaelev2", salt),
        phone: "0700111222",
        startYear: 2016,
        role: AccessRole.StudentRole
    };

    Object.assign(payload1);
    Object.assign(payload2);

    const newStudent1: User = await new UserModel(payload1).save();
    const newStudent2: User = await new UserModel(payload2).save();

    const role1: Role = await new RoleModel({
        userId: newStudent1._id,
        roleLevel: payload1.role as AccessRole
    }).save();

    const role2: Role = await new RoleModel({
        userId: newStudent2._id,
        roleLevel: payload2.role as AccessRole
    }).save();

    const student1Dto: UserDTO = new UserDTO();
    Object.assign(student1Dto, { role1 })
    const student2Dto: UserDTO = new UserDTO();
    Object.assign(student2Dto, { role2 })

    const p1: any = UserModel.findByIdAndUpdate(newStudent1._id, { $set: { role: role1._id } }, { new: true });
    const p2: any = UserModel.findByIdAndUpdate(newStudent2._id, { $set: { role: role2._id } }, { new: true });

    return Promise.all([p1, p2]);
}

async function makeProfGroup(professorIds: string[]): Promise<Group> {
    const payload: any = {
        name: "Profesori",
        students: professorIds,
        assignments: []
    };

    const newGroup: Group = await new GroupModel(payload).save();

    for (const id of professorIds) {
        UserModel.findByIdAndUpdate(professorIds, { $set: { group: newGroup._id } }, { new: true });
    }

    return newGroup;
}

async function makeGroup(studentIds: string[]): Promise<Group> {
    const payload: any = {
        name: "332",
        students: studentIds,
        assignments: []
    };

    const newGroup: Group = await new GroupModel(payload).save();

    for (const id of studentIds) {
        await UserModel.findByIdAndUpdate(id, { $set: { group: newGroup._id.toHexString() } }, { new: true });
    }

    return newGroup;
}

async function makeDefaultFaculty(groups: string[], professors: string[]): Promise<Faculty> {
    const payload: any = {
        name: "Facultatea de Informatica",
        address: "Piata Universitatii",
        subjects: [],
        groups: groups,
        professors: professors
    };

    return await new FacultyModel(payload).save();
}

async function makeSubjects(professorId: string, faculty: string): Promise<Subject> {
    const payload: any = {
        name: "Dezvoltarea Aplicatiilor Web",
        year: 3,
        timetable: {
            startDate: "2021-02-01T14:17:27.811Z",
            endDate: "2021-07-05T14:17:27.811Z",
            cron: "0 10 * * 1,3"
        },
        semester: "semestrul 1",
        credits: 30,
        professor: professorId,
        assignments: []
    };

    const subject: Subject = await new SubjectModel(payload).save();

    await FacultyModel.findByIdAndUpdate(faculty, { $push: { subjects: subject._id.toHexString() } }, { new: true });

    return subject;
}

async function build(): Promise<void> {
    try {
        console.log("Creating default administrator...")
        const admin: User = await makeAdmin();
        console.log("Creating default Professors...")
        const professors: User[] = await makeProfessors();
        console.log("Creating default Professor group...")
        const professorGroup: Group = await makeProfGroup([
            admin._id.toHexString(),
            ...professors.map(p => p._id.toHexString())
        ]);
        console.log("Creating default Students...")
        const students: User[] = await makeStudents();
        console.log("Creating default Student group...")
        const studentGroup: Group = await makeGroup(students.map(p => p._id.toHexString()));
        console.log("Creating default Faculty...")
        const faculty: Faculty = await makeDefaultFaculty([
            professorGroup._id.toHexString(),
            studentGroup._id.toHexString()
        ], [
            admin._id.toHexString(),
            ...professors.map(p => p._id.toHexString())
        ]);
        console.log("Creating default Subjects...");
        await makeSubjects(professors[0]._id.toHexString(), faculty._id.toHexString())

        process.exit(0);
    } catch (e) {
        console.error(e);
        process.exit(0);
    }

}

build();