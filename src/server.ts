/**
 * @packageDocumentation
 * @module App
 */

import * as path from "path";
import * as express from "express";
import bodyParser from 'body-parser';
import compression from 'compression';
import cors from 'cors';
import cookieParser from "cookie-parser";
import {Configuration} from "./utils/Configuration";
import {NestFactory} from "@nestjs/core";
import {AppModule} from "./AppModule";
import {NestExpressApplication} from "@nestjs/platform-express";

const config: Configuration = Configuration.getInstance();

/**
 * Start up the server
 */
async function bootstrap(): Promise<void> {
    const app: NestExpressApplication = await NestFactory.create<NestExpressApplication>(AppModule);
    const port: number = config.getExpressConfig().port;

    // Configuration
    // tslint:disable-next-line
    app.use(bodyParser.json({ limit: "50mb" })); // Body parser middleware
    app.use(cookieParser());
    app.use(cors()); // CORS configurations
    app.use(compression()); // GZip compression middleware
    app.setViewEngine("pug");
    app.setBaseViewsDir(path.join(__dirname, "ui/views")); // View storage

    // Static resources
    app.use(express.static(path.join(__dirname, "ui", "public")))

    await app.listen(port, () => {
        console.log(`Started the server on port ${port}`);
    });
}

// Bootstrap application
bootstrap();