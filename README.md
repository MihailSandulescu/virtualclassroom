## virtual-classroom

#### Prerequisites

You will need to have [NodeJS >= 10](https://nodejs.org/en/download/) and [Yarn](https://yarnpkg.com/) installed in order to get started.

You will also need a MongoDB database. You can self-host or you can use a tool like [mLab](https://mlab.com/). Add authentication details to `config/config.yml`.

#### How to build

In order to build this project for production, run `yarn run build`.

In order to build this project with source maps for development, run `yarn run build:debug`.

The transpiled project will be found under `build/`

#### How to run

When you run the app, you need to specify the environment you're on. You can do this by setting the `NODE_ENV` environment variable to `prod`, `test` or `local`.

To run the app in development mode, use `yarn run start:dev`. 

#### How to get documentation

To generate documentation locally, you can run `yarn run generate-docs`. The documentation can be found under `docs/`.

#### Schema

![](schema.jpg)
